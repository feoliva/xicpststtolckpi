# file /afs/cern.ch/user/f/feoliva/GaussDev_v49r17/Gen/DecFiles/options/25203000.py generated: Sun, 21 Mar 2021 02:30:41
#
# Event Type: 25203000
#
# ASCII decay Descriptor: ${Lc}[Lambda_c+ ==> ${Lcp}p+ ${LcK}K- ${Lcpi}pi+]CC
#
from Configurables import Generation
Generation().EventType = 25203000
Generation().SampleGenerationTool = "SignalPlain"
from Configurables import SignalPlain
Generation().addTool( SignalPlain )
Generation().SignalPlain.ProductionTool = "Pythia8Production"
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/Lc_pKpi-res=LHCbAcceptance.dec"
Generation().SignalPlain.CutTool = "LHCbAcceptance"
Generation().SignalPlain.SignalPIDList = [ 4122,-4122 ]
