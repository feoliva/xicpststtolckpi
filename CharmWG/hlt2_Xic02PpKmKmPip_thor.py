"""
Xic0-> p K- K- pi+

Reconstruction
----------
1. Reconstruct tracks
2. Identify tracks (protons, kaons and pions)
3. Combine kaons, proton and pion to form Xic0 candidates 

"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad
import math
import Functors as F
from Functors.math import in_range 
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter, require_all
from Hlt2Conf.standard_particles import make_has_rich_long_protons,  make_has_rich_long_kaons, make_has_rich_long_pions
from Moore import options, run_moore
from Moore.lines import Hlt2Line
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs_v2 as make_pvs
from Hlt2Conf.algorithms import ParticleFilterWithPVs

#a more general make_selected_particle should be better 


def make_selected_kaons(kaons, pvs):
    cut = require_all(
       F.PT > 500.0 * MeV,
       F.MINIPCHI2(pvs)>4.0,
    )
    filtered_kaons=ParticleFilter(
       kaons,
       F.FILTER(cut),
    )
    return filtered_kaons

def make_selected_pions(pions, pvs):
    cut = require_all(
       F.PT > 500.0 * MeV,
       F.MINIPCHI2(pvs)>4.0,
    )
    filtered_pions=ParticleFilter(
       pions,
       F.FILTER(cut),
    )
    return filtered_pions

def make_selected_protons(protons, pvs):
    cut = require_all(
       F.PT > 500.0 * MeV,
       F.MINIPCHI2(pvs)>4.0,
    )
    filtered_protons=ParticleFilter(
       protons,
       F.FILTER(cut),
    )
    return filtered_protons

def make_xic0(protons, 
              kaons, 
              pions,
              pvs, 
              am_min= 2386.0 * MeV, 
              am_max= 2780.0 * MeV, 
              dira  = 35     * mrad):
   #require_all(functor1, functor2,..)
   combination_cut= require_all(
   in_range(am_min, F.COMPOSITEMASS, am_max), 
   F.SUM(F.PT)> 3000.0 * MeV,     #((APT1+APT2+APT3+APT4) > 3000.0 )
   F.SUM(F.PT > 1000.0 * MeV)>0,  #(AHASCHILD(PT > 1000.0))
   F.SUM(F.PT >  500.0 * MeV)>=2, #(ANUM(PT > 500.0) >= 2)
   F.SUM(F.MINIPCHI2(pvs)>8.0)>0,   #(AHASCHILD((MIPCHI2DV(PRIMARY)) > 8.0))
   F.SUM(F.MINIPCHI2(pvs)>6.0)>=2, #(ANUM(MIPCHI2DV(PRIMARY) > 6.0) >= 2)
   )
   vertex_cut = require_all(
   F.CHI2/F.CHI2DOF <10.0, #"(VFASPF(VCHI2PDOF) < 10.0)", 
   F.BPVDIRA(pvs)>math.cos(dira), #"(BPVDIRA > lcldira)",  #to check!
   )
   xic0 = ParticleCombiner(
      DecayDescriptors=["[Xi_c0 -> p+ K- K- pi+]cc"],
      #particles=particles,
      Inputs = [protons, kaons, pions],
      #Thor functors
      Comb1234Cut=combination_cut,
      MotherCut=vertex_cut,
    )
   return xic0


def xic0_to_pKmKmpip():
   pvs = make_pvs()
   protons= make_has_rich_long_protons()
   kaons  = make_has_rich_long_kaons()
   pions  = make_has_rich_long_pions()
   filtered_protons= make_selected_protons(protons, pvs)
   filtered_kaons  = make_selected_kaons(kaons, pvs)
   filtered_pions  = make_selected_pions(pions, pvs)
#  xicp= make_xicp( particles=[filtered_protons, filtered_kaons, filtered_pions])
   xic0= make_xic0(filtered_protons, filtered_kaons, filtered_pions, pvs)

   return Hlt2Line(
    name="Hlt2Xc0TopKmKmPipLine",
    algs=[xic0],
    prescale=1.0,
    )


def make_lines():
    return[xic0_to_pKmKmpip()]

options.evt_max = 10
public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    run_moore(options, make_lines, public_tools)
