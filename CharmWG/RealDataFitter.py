###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import logging
import ROOT as R
import awkward as ak
import numpy as np

from Fitter import Fitter
from DataLibrary import DataLibrary
from Utils import plot


logging.basicConfig(level=logging.NOTSET)
logger = logging.getLogger("RealDataFitter")


class RealDataFitter(Fitter):
    def __init__(
        self, DATA: DataLibrary, channel: str, fitting_configuration: dict
    ) -> None:
        Fitter.__init__(self, DATA, channel, fitting_configuration)

        self._fit_type = "DATA"

        # define the binning scheme from analysis constants internally
        from analysis_constants import pt_bins, rapidity_bins, bin_name

        self._bins = dict()
        for rapidity_bin in rapidity_bins:
            for pt_bin in pt_bins:
                bin = bin_name(rapidity_bin, pt_bin)
                # the bins below are the bin integrated "bins" and we dont
                # want them defined along with the bins
                if "_2_0to4_5_" not in bin and "_0to15000" not in bin:
                    self._bins[bin] = {
                        "rapidity": rapidity_bin,
                        "pt": pt_bin,
                    }
        # TODO: remove this when have more data:
        self._bins = dict()
        self._bins["bin_2to4_5_0to5000"] = {
            "rapidity": [2, 4.5],
            "pt": [0, 5000],
        }
        self._bins["bin_2to4_5_5000to10000"] = {
            "rapidity": [2, 4.5],
            "pt": [5000, 10000],
        }
        self._bins["bin_2to4_5_10000to15000"] = {
            "rapidity": [2, 4.5],
            "pt": [10000, 15000],
        }
    #    self._bins["bin_2to3_25_0to15000"] = {
    #        "rapidity": [2, 3.25],
    #        "pt": [0, 15000],
    #    }
    #    self._bins["bin_3_25to4_5_0to15000"] = {
    #        "rapidity": [3.25, 4.5],
    #        "pt": [0, 15000],
    #    }

        # create extra storage dictionaries for simultaneous fitting
        self._roo_bin_mass_datas = dict()
        self._roo_bin_datas = dict()
        self._roo_bin_PDFs = dict()

    def read_workspace(self) -> None:
        # read the workspace created from the Monte Carlo fitting from file

        workspace_file = R.TFile(
            f"{os.environ['CHARMROOT']}/analysis/fitting/workspaces/{self._version}/{self._channel}.root"
        )
        workspace = workspace_file.Get("workspace")

        # prompt distribution
        self._roo_parameters["mu_prompt"] = workspace.var("mu_prompt")
        self._roo_parameters["sigma_prompt"] = workspace.var("sigma_prompt")
        self._roo_parameters["epsilon_prompt"] = workspace.var("epsilon_prompt")
        self._roo_parameters["rhoL_prompt"] = workspace.var("rhoL_prompt")
        self._roo_parameters["rhoR_prompt"] = workspace.var("rhoR_prompt")

        # secondary distribution
        self._roo_parameters["mu_secondary"] = workspace.var("mu_secondary")
        self._roo_parameters["sigma_secondary"] = workspace.var("sigma_secondary")

        # combinatorial background distribution
        self._roo_combinatorial_background = workspace.pdf("combinatorial_background")

    def set_channel_specifics(self) -> None:
        # this function sets the parameters which are fixed or
        # split in each bin which is decay channel specific

        # need to fix the parameters which are marked as "fix"
        for parameter in self._channel_specifics["fix"]:
            self._roo_parameters[parameter].setConstant()

        # need to duplicate the parameters which are marked as "split"
        # for each bin in the simultaneous fit
        for bin in self._bins.keys():
            for parameter in self._channel_specifics["split"]:
                name = f"{parameter}_{bin}"
                self._roo_parameters[name] = R.RooRealVar(
                    self._roo_parameters[parameter], name
                )
            nbins = len(self._bins)
            self._roo_parameters[f"N_signal_mass_{bin}"] = R.RooRealVar(
                f"N_signal_mass_{bin}",
                f"N_signal_mass_{bin}",
                self._roo_parameters["N_signal_mass"].getValV() / nbins,
                0,
                self._roo_parameters["N_signal_mass"].getValV(),
            )
            self._roo_parameters[f"N_background_mass_{bin}"] = R.RooRealVar(
                f"N_background_mass_{bin}",
                f"N_background_mass_{bin}",
                self._roo_parameters["N_background_mass"].getValV() / nbins,
                0,
                self._roo_parameters["N_background_mass"].getValV(),
            )
            self._roo_parameters[f"N_prompt_{bin}"] = R.RooRealVar(
                f"N_prompt_{bin}",
                f"N_prompt_{bin}",
                self._roo_parameters["N_prompt"].getValV() / nbins,
                0,
                self._roo_parameters["N_prompt"].getValV(),
            )
            self._roo_parameters[f"N_secondary_{bin}"] = R.RooRealVar(
                f"N_secondary_{bin}",
                f"N_secondary_{bin}",
                self._roo_parameters["N_secondary"].getValV() / nbins,
                0,
                self._roo_parameters["N_secondary"].getValV(),
            )
            self._roo_parameters[f"N_background_{bin}"] = R.RooRealVar(
                f"N_background_{bin}",
                f"N_background_{bin}",
                self._roo_parameters["N_background_mass"].getValV() / nbins,
                0,
                self._roo_parameters["N_background_mass"].getValV(),
            )

    ########### TODO temporary fix for this production!!!
    def check_for_RAP_branch(self) -> None:
        if (
            f"{self._fitting_configuration['fit_particle']}_RAP"
            not in self._data_library._data_frame.fields
        ):
            print("no RAP!!")
            self._data_library._data_frame[
                f"{self._fitting_configuration['fit_particle']}_RAP"
            ] = np.log(
                (
                    np.sqrt(
                        pow(
                            self._data_library._data_frame[
                                f"{self._fitting_configuration['fit_particle']}_M"
                            ],
                            2,
                        )
                        + pow(
                            self._data_library._data_frame[
                                f"{self._fitting_configuration['fit_particle']}_PT"
                            ]
                            * np.cosh(
                                self._data_library._data_frame[
                                    f"{self._fitting_configuration['fit_particle']}_ETA"
                                ]
                            ),
                            2,
                        )
                    )
                    + self._data_library._data_frame[
                        f"{self._fitting_configuration['fit_particle']}_PT"
                    ]
                    * np.sinh(
                        self._data_library._data_frame[
                            f"{self._fitting_configuration['fit_particle']}_ETA"
                        ]
                    )
                )
                / np.sqrt(
                    pow(
                        self._data_library._data_frame[
                            f"{self._fitting_configuration['fit_particle']}_M"
                        ],
                        2,
                    )
                    + pow(
                        self._data_library._data_frame[
                            f"{self._fitting_configuration['fit_particle']}_PT"
                        ],
                        2,
                    )
                )
            )
        else:
            print("RAP!!")

    #####################################################

    def make_simultaneous_fit_data(self) -> None:
        # this function performs the data binning in preparation for the simulatneous fit

        self.check_for_RAP_branch()  # TODO

        # loop through each bin and cut data based on the bin edges
        for bin in self._bins.keys():
            selection = [True for i in range(len(self._data_library._data_frame))]
            the_selection = (
                self._data_library._data_frame[self._PT_variable]
                > self._bins[bin]["pt"][0]
            )
            selection = selection & the_selection
            selection = selection & (
                self._data_library._data_frame[self._PT_variable]
                < self._bins[bin]["pt"][1]
            )
            selection = selection & (
                self._data_library._data_frame[self._RAP_variable]
                > self._bins[bin]["rapidity"][0]
            )
            selection = selection & (
                self._data_library._data_frame[self._RAP_variable]
                < self._bins[bin]["rapidity"][1]
            )
            binned_data_frame = self._data_library._data_frame[selection]

            # store the binned RooDataSets
            self._roo_bin_mass_datas[bin] = self.get_roo_data(
                binned_data_frame,
                [self._roo_parameters["m"]],
            )
            self._roo_bin_datas[bin] = self.get_roo_data(
                binned_data_frame,
                [self._roo_parameters["x"]],
            )

    def make_simultaneous_mass_fit_model(self) -> None:
        # this function creates the mass PDFs for each bin in the simultaneous fit

        # create a simultaneous fit PDF list
        self._mass_fit_bins = R.RooCategory("mass_fit_bins", "mass_fit_bins")
        self._simultaneous_mass_fit_model = R.RooSimultaneous(
            "simultaneous_mass_fit_model",
            "simultaneous_mass_fit_model",
            self._mass_fit_bins,
        )

        self.models = dict()  # need this to stop memory loss and run time crash
        for bin in self._bins.keys():
            self._mass_fit_bins.defineType(bin)

            self.models[bin] = self._make_mass_PDF[self._channel](bin)
            self._roo_bin_PDFs[f"signal_mass_{bin}"] = self.add_PDFs(
                R.RooArgList(*self.models[bin]["signal"]), bin
            )
            self._roo_bin_PDFs[f"background_mass_{bin}"] = self.add_PDFs(
                R.RooArgList(*self.models[bin]["background"]), bin, is_background=True
            )
            # TODO: do i need to add these to the dict or just the totals
            self._roo_bin_PDFs[f"total_mass_fit_model_{bin}"] = {
                "total": R.RooAddPdf(
                    f"total_mass_fit_model_{bin}",
                    "Total",
                    R.RooArgList(
                        self._roo_bin_PDFs[f"signal_mass_{bin}"],
                        self._roo_bin_PDFs[f"background_mass_{bin}"],
                    ),
                    R.RooArgList(
                        self._roo_parameters[f"N_signal_mass_{bin}"],
                        self._roo_parameters[f"N_background_mass_{bin}"],
                    ),
                ),
                "signals": {
                    signal.GetName(): signal.GetTitle()
                    for signal in self.models[bin]["signal"]
                },
                "backgrounds": {
                    background.GetName(): background.GetTitle()
                    for background in self.models[bin]["background"]
                },
            }

            # add the fit PDF to the list of simultaneous fit PDFs
            self._simultaneous_mass_fit_model.addPdf(
                self._roo_bin_PDFs[f"total_mass_fit_model_{bin}"]["total"], bin
            )

    def make_simultaneous_fit_model(self) -> None:
        # this function creates the lnIPchi2 PDFs for each bin in the simultaneous fit

        # create a simultaneous fit PDF list
        self._fit_bins = R.RooCategory("fit_bins", "fit_bins")
        self._simultaneous_fit_model = R.RooSimultaneous(
            "simultaneous_fit_model", "simultaneous_fit_model", self._fit_bins
        )

        for bin in self._bins.keys():
            self._fit_bins.defineType(bin)

            # prompt
            self._roo_bin_PDFs[f"signal_prompt_{bin}"] = self.make_prompt_PDF(bin)

            # secondary
            self._roo_bin_PDFs[f"signal_secondary_{bin}"] = self.make_secondary_PDF(bin)

            # combinatorial background
            self._roo_bin_PDFs[
                f"combinatorial_background_{bin}"
            ] = self._roo_combinatorial_background

            # need to create an additional background yield constraint for each bin
            self._roo_bin_PDFs[f"background_constraint_{bin}"] = R.RooGaussian(
                f"background_constraint_{bin}",
                f"background_constraint_{bin}",
                self._roo_parameters[f"N_background_{bin}"],
                R.RooFit.RooConst(
                    self._roo_parameters[f"N_background_mass_{bin}"].getValV()
                ),
                R.RooFit.RooConst(
                    self._roo_parameters[f"N_background_mass_{bin}"].getError()
                ),
            )
            self._roo_bin_PDFs[f"unconstrained_model_{bin}"] = R.RooAddPdf(
                f"unconstrained_model_{bin}",
                "Total",
                R.RooArgList(
                    self._roo_bin_PDFs[f"signal_prompt_{bin}"],
                    self._roo_bin_PDFs[f"signal_secondary_{bin}"],
                    self._roo_bin_PDFs[f"combinatorial_background_{bin}"],
                ),
                R.RooArgList(
                    self._roo_parameters[f"N_prompt_{bin}"],
                    self._roo_parameters[f"N_secondary_{bin}"],
                    self._roo_parameters[f"N_background_{bin}"],
                ),
            )

            # total
            self._roo_bin_PDFs[f"total_fit_model_{bin}"] = {
                "total": R.RooProdPdf(
                    f"total_fit_model_{bin}",
                    "Total",
                    R.RooArgSet(
                        self._roo_bin_PDFs[f"background_constraint_{bin}"],
                        self._roo_bin_PDFs[f"unconstrained_model_{bin}"],
                    ),
                ),
                "signals": {
                    self._roo_bin_PDFs[f"signal_prompt_{bin}"]
                    .GetName(): self._roo_bin_PDFs[f"signal_prompt_{bin}"]
                    .GetTitle(),
                    self._roo_bin_PDFs[f"signal_secondary_{bin}"]
                    .GetName(): self._roo_bin_PDFs[f"signal_secondary_{bin}"]
                    .GetTitle(),
                },
                "backgrounds": {
                    self._roo_bin_PDFs[f"combinatorial_background_{bin}"]
                    .GetName(): self._roo_bin_PDFs[f"combinatorial_background_{bin}"]
                    .GetTitle()
                },
            }

            # add the fit PDF to the list of simultaneous fit PDFs
            self._simultaneous_fit_model.addPdf(
                self._roo_bin_PDFs[f"total_fit_model_{bin}"]["total"], bin
            )

    def do_simultaneous_mass_fit(self) -> None:
        # this function will be called by the do_simultaneous_fit
        # function before it carries out the lnIPchi2 fit

        # make the simultaneous model
        self.make_simultaneous_mass_fit_model()

        # get the binned data
        imports = [
            R.RooFit.Import(name, data)
            for name, data in self._roo_bin_mass_datas.items()
        ]
        roo_data = R.RooDataSet(
            "roo_data",
            "combined data",
            R.RooArgSet(self._roo_parameters["m"]),
            R.RooFit.Index(self._mass_fit_bins),
            *imports,
        )

        # do the fit!
        fit = self._simultaneous_mass_fit_model.fitTo(
            roo_data,
            R.RooFit.Extended(True),
            R.RooFit.Save(),
            R.RooFit.Minos(0),
        )
        fit.SetName(f"{self._fit_type} simultaneous mass")
        self.check_fit_success(fit)

        # make the plots
        for bin in self._bins.keys():
            plot(
                self._roo_parameters["m"],
                self._roo_bin_mass_datas[bin],  # self._roo_bin_mass_datas[f"{bin}_m"],
                self._roo_bin_PDFs[f"total_mass_fit_model_{bin}"],
                save_to=f"{os.environ['CHARMROOT']}/analysis/fitting/plots/{self._channel}/{self._version}/simultaneous_fit/mass_{bin}",
            )

    def do_simultaneous_fit(self) -> None:
        # set the specifics for the fitting channel
        self.set_channel_specifics()

        # create both the mass and lnIPchi2 fitting data
        self.make_simultaneous_fit_data()

        # the mass fit must be done first
        self.do_simultaneous_mass_fit()

        # make the simultaneous model
        self.make_simultaneous_fit_model()

        # get the binned data
        imports = [
            R.RooFit.Import(name, data) for name, data in self._roo_bin_datas.items()
        ]
        roo_data = R.RooDataSet(
            "roo_data",
            "combined data",
            R.RooArgSet(self._roo_parameters["x"]),
            R.RooFit.Index(self._fit_bins),
            *imports,
        )

        # do the fit!
        fit = self._simultaneous_fit_model.fitTo(
            roo_data,
            R.RooFit.Extended(True),
            R.RooFit.Save(),
            R.RooFit.Minos(0),
        )
        fit.SetName(f"{self._fit_type} simultaneous lnIPchi2")
        self.check_fit_success(fit)
        # make the plots
        for bin in self._bins.keys():
            plot(
                self._roo_parameters["x"],
                self._roo_bin_datas[bin],  # self._roo_bin_datas[f"{bin}_x"],
                self._roo_bin_PDFs[f"total_fit_model_{bin}"],
                save_to=f"{os.environ['CHARMROOT']}/analysis/fitting/plots/{self._channel}/{self._version}/simultaneous_fit/ln_IPchi2_{bin}",
            )
            #with open(f"{bin}.txt", 'a') as g:
            with open(f"MassFit_Values.txt", 'a') as g:
               # g.write(str(self._roo_parameters[f"N_signal_mass_{bin}"].getValV()))
               g.write(f"{bin}"+'  ')
               g.write(str(self._roo_parameters[f"N_signal_mass_{bin}"].getValV())+'\n')
               
