"""
Xic+-> p K- pi+

Reconstruction
----------
1. Reconstruct tracks
2. Identify which tracks are kaons and which are pions
3. Combine kaon, proton and pion to form Xic+ candidates 

"""

from GaudiKernel.SystemOfUnits import GeV, mm
import math
import Functors as F
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter, require_all
from Hlt2Conf.standard_particles import make_has_rich_long_protons,  make_has_rich_long_kaons, make_has_rich_long_pions
from Moore import options, run_moore
from Moore.lines import Hlt2Line
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from PyConf import configurable

#a more general make_selected_particle should be better 

def make_selected_kaons(kaons):
    cut = require_all(
       F.PT > 0.2 * GeV,
    )
    filtered_kaons=ParticleFilter(
       kaons,
       F.FILTER(cut),
    )
    return filtered_kaons

def make_selected_pions(pions):
    cut = require_all(
       F.PT > 0.2 * GeV,
    )
    filtered_pions=ParticleFilter(
       pions,
       F.FILTER(cut),
    )
    return filtered_pions

def make_selected_protons(protons):
    cut = require_all(
       F.PT > 0.2 * GeV,
    )
    filtered_protons=ParticleFilter(
       protons,
       F.FILTER(cut),
    )
    return filtered_protons

def make_xicp(particles):
   #require_all(functor1, functor2,..)
   combination_cut= require_all(
   "(in_range( 2201.0, AM, 2553.0 ))", 
   "((APT1+APT2+APT3) > 3000.0 )",
   "(AHASCHILD(PT > 1000.0))",
   "(ANUM(PT > 400.0) >= 2)", 
   "(AHASCHILD((MIPCHI2DV(PRIMARY)) > 16.0))", 
   "(ANUM(MIPCHI2DV(PRIMARY) > 9.0) >= 2)",
   )
   vertex_cut = require_all(
   "(VFASPF(VCHI2PDOF) < 10.0)", 
   "(BPVDIRA > lcldira)",
   "(BPVLTIME() > 0.00015)",
   )
   xicp = ParticleCombiner(
      DecayDescriptors=['[Xic+ -> p K- pi+]cc'],
      particles=particles,
      #Loki Functors
      #Comb12Cut="APT > 2* GeV",
      #Thor functors
      CombCut=combination_cut,
      MotherCut=vertex_cut,
    )
   return xicp


def xicp_to_pKmpip():
   protons= make_has_rich_long_protons()
   kaons  = make_has_rich_long_kaons()
   pions  = make_has_rich_long_pions()
   filtered_protons= make_selected_protons(protons)
   filtered_kaons  = make_selected_kaons(kaons)
   filtered_pions  = make_selected_pions(pions)
   xicp= make_xicp( particles=[filtered_protons, filtered_kaons, filtered_pions])
   return Hlt2Line(
    name="Hlt2XcpTopKmPipLine",
    algs=[xicp],
    prescale=1.0,
    )


def make_lines():
    return[xicp_to_pKmpip()]

options.evt_max = 10
public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    run_moore(options, make_lines, public_tools)
