#include <iostream>
#include <cmath>
#include "Rtypes.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TROOT.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TRandom.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TString.h"
#include "Riostream.h"
#include "TMath.h"
#include <string.h>
#include "TMath.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TCut.h"
#include "TFile.h"
#include "RooPlot.h"
#include "TAxis.h"
#include "RooHist.h"
#include "TLatex.h"
#include <sstream>
#include <utility>
#include "TStyle.h"
#include "TROOT.h"
#include "TLine.h"
#include "TCut.h"
#include "TRandom.h"

void randomEvents()
{

//gStyle->SetOptFit(1);
//TFile *hfile = new TFile("RandomEvents_MagDown2016.root","RECREATE");
//TFile *hfile1 = new TFile("RandomEvents_MagDown2016.root","RECREATE");
TFile *hfile1 = new TFile("RandomEvents_MagUp2016.root","RECREATE");
//TFile *hfile2 = new TFile("RandomEvents_MagDown2016_odd.root","RECREATE");

TRandom3 *r1=new TRandom3();

//for (int jfile=0; jfile!=NFiles; jfile++){

//FilesInput>>InputFile;

//cout << "InputFile:  " <<InputFile<<endl;
//TFile *f1 = TFile::Open("/eos/lhcb/user/f/feoliva/Data2016_Mag_Down_Lc2PKPi.root");
TFile *f1 = TFile::Open("/eos/lhcb/user/f/feoliva/Data2016_Mag_Up_Lc2PKPi.root");
TTree * t1 =(TTree*)f1->Get("DecayTree");

Int_t nentries = (Int_t)t1->GetEntries();

cout << "The number of entries is :  " <<nentries<<endl;
//
//
//


hfile1->cd();

auto tree1 = t1->CloneTree(0,"fast");
//auto tree2 = t1->CloneTree(0,"fast");
//auto treew = t1->CloneTree(0);
//ULong64_t eventNumber;
//t1->SetBranchAddress("eventNumber",&eventNumber);

//for (int i=0; i<int(nentries*0.05); i++){
for (int i=0; i<int(nentries); i++){
//for (int i=0; i<13; i++){
if(r1->Rndm()<0.05) 
//int j = r1->Uniform(0, nentries);
{
t1->GetEntry(i);

//if(eventNumber%2==1)  tree1->Fill();
//if(eventNumber%2==0)  tree2->Fill();
//t1->GetEntry(j);
//treew->Fill();
tree1->Fill();
}

}

tree1->Write();
hfile1->Close();


//TFile *f3 = TFile::Open("RandomEvents_MagDown2016.root");
//TTree *decaytree = (TTree *)f1->Get("DecayTree");
/*
TFile *hodd = new TFile("RandomEvents_MagDown2016_Odd.root","RECREATE");
TFile *heven = new TFile("RandomEvents_MagDown2016_Even.root","RECREATE");

TCut odd = "eventNumber%2!=1";
hodd->cd();
//TTree *treeodd = decaytree->CopyTree(odd); //mc
TTree *treeodd = t1->CopyTree(odd); //mc
treeodd->Write();
hodd->Close();

TCut even = "eventNumber%2==1";
heven->cd();
//TTree *treeeven = decaytree->CopyTree(even); //mc
TTree *treeeven = t1->CopyTree(even); //mc
treeeven->Write();
heven->Close();
*/

return;
}
