#include <iostream>
#include <cmath>
#include "Rtypes.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TROOT.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TRandom.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TString.h"
#include "Riostream.h"
#include "TMath.h"
#include <string.h>
#include "RooExponential.h"
#include "TMath.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TCut.h"
#include "TFile.h"
#include "RooPlot.h"
#include "TAxis.h"
#include  "RooHist.h"
#include "TLatex.h"
#include <sstream>
#include <utility>
#include "TStyle.h"
#include "TROOT.h"
#include "TLine.h"
#include "RooStats/SPlot.h"
#include "RooStats/RooStatsUtils.h" 
#include "TBranch.h"
ClassImp(RooStats::SPlot) ;

using namespace RooFit ;
using namespace RooStats;



void macrofit_sweights_range()
{

gStyle->SetOptFit(1);
TH1F *h1 =new TH1F("h1","Lc Invariant Mass distribution",200,2240, 2340);
TH1F *h2 =new TH1F("h2","Weights",200,-2, 2);
int NFiles;

ifstream FilesInput;
char InputFile[1000];
char fname[1000];

TCanvas *c = new TCanvas("c", "c", 800,860);

//FilesInput.open("FilesInput_Lc2KPip.card",ifstream::in);
//FilesInput.open("FilesInput3.card",ifstream::in);
//FilesInput.open("FilesInput_may.card",ifstream::in);
//FilesInput.open("FilesInputTotal.card",ifstream::in);

FilesInput.open("datasampleodd_2016",ifstream::in);
//FilesInput.open("datasampleeven_2016",ifstream::in);
//TFile *rootfile = new TFile("Lc.root","recreate");

//TTree *treeLc = new TTree("treeLc","treeLc");

FilesInput >> NFiles;

float min,max;
min=2250;
max=2320;
cout << "NFiles: " <<NFiles << endl;

float meangaus, sigmagaus;

for (int jfile=0; jfile!=NFiles; jfile++){

FilesInput>>InputFile;

cout << "InputFile:  " <<InputFile<<endl;
TFile *f1 = TFile::Open(InputFile);

TTree * t1 =(TTree*)f1->Get("DecayTree");
//TTree * t1 =(TTree*)f1->Get("DecayTree");
//f1->cd("Lc2PKPiTree");
//TDirectory* dir1 = t1 ->GetDirectory("Lc2PKPiTree");

//TTree* t1 = (TTree*)dir1->Get("Lc2PKPiTree/DecayTree");
//print(ROOT.RDataFrame(treeName, fileName).GetColumnNames())

//Check of the variables
Double_t Lc_M;
//Double_t Lc_M_DTF_PV;

t1->SetBranchAddress("Lc_M",&Lc_M);
//t1->SetBranchAddress("Lc_M_DTF_PV",&Lc_M_DTF_PV);

Int_t nentries = (Int_t)t1->GetEntries();
//std::cout << "\n\nReading entries:  "<<nentries<<std::endl;
for (int i=0; i<nentries; i++){
t1->GetEntry(i);
h1->Fill(Lc_M);
if(min>Lc_M) min=Lc_M;
if(max<Lc_M) max=Lc_M;

//treeLc->Branch("Lc_M",&Lc_M);
//treeLc->Fill();
//h1->Fill(Lc_M_DTF_PV);
}

//}
TF1 *gaus = new TF1("gaus","gaus", 2240, 2340);
h1->Fit(gaus, "","", 2280, 2296);
//h1->Fit(gaus, "","", 2275, 2300);
h1->Draw("");
meangaus=gaus->GetParameter(1);
sigmagaus=gaus->GetParameter(2);

//treeLc->Write();

//RooRealVar m("Lc_M","Lc_M", 2230, 2340);
//RooRealVar m("Lc_M","Lc_M", 2210, 2370);
//RooRealVar m("Lc_M","Lc_M", min, max);
RooRealVar m("Lc_M","Lc_M", 2211, 2362);
//RooRealVar m("Lc_M_DTF_PV","Lc_M_DTF_PV", 2230, 2340);
RooDataSet data("data","data",RooArgSet(m),Import(*t1)) ;
//RooDataSet data("data","data",RooArgSet(m),Import(*treeLc)) ;

/////Gaussian Fit for the signal/////

//std::cout << "\n Mean gaus " << meangaus << std::endl;

//Background model

RooRealVar a0("a0","a0",-0.5,-0.6,0.3) ;
//RooRealVar a1("a1","a1",-0.1,-0.5,0.5) ;
//RooRealVar a2("a2","a2",-0.1,-0.5,0.5) ;
RooChebychev bkg("bkg","bkg",m,RooArgList(a0));

//RooRealVar nlc("nlc", "nlc", 80000, 0, 140000); 
//RooRealVar nlc("nlc", "nlc", 917000, 0, 1600000); 
//RooRealVar nlc("nlc", "nlc", 900000, 0, 1600000); 
//RooRealVar nlc("nlc", "nlc", 800000, 0, nentries); 
//RooRealVar nlc("nlc", "nlc", 870000, 0, nentries); 
RooRealVar nlc("nlc", "nlc", 10000,  0, nentries); 


//RooRealVar nback("nback", "nback", 60000, 0, 140000);
//RooRealVar nback("nback", "nback", 656000, 0, 1600000);
//RooRealVar nback("nback", "nback", 700000, 0, 1600000);
//RooRealVar nback("nback", "nback", 700000, 0, nentries);
//RooRealVar nback("nback", "nback", 700000, 0, nentries);
//RooRealVar nback("nback", "nback", 50000, 0, nentries);
RooRealVar nback("nback", "nback", 10000, 0, nentries);
//RooRealVar nback("nback", "nback", 10000, 0, nentries*0.6);

//Signal model

RooRealVar  meang("meang", "Lc mass", meangaus, 2280, 2360);
//RooRealVar  sigmag("sigmag", "Lc mass width", sigmagaus, 0, 10);
//RooRealVar  sigmag("sigmag", "Lc mass width", sigmagaus, 0, 30);
//RooRealVar  sigmag("sigmag", "Lc mass width", sigmagaus, 0, 50);
RooRealVar  sigmag("sigmag", "Lc mass width", sigmagaus, 0, 30);

RooGaussian gauss("gauss","gauss",m, meang,sigmag);
//RooRealVar sigcb("sigcb", "sigcb", 5, 0, 50);
RooRealVar sigcb("sigcb", "sigcb", 5, 0, 20);
//RooRealVar sigcb("sigcb", "sigcb", 2, 0, 15);
//RooRealVar sigcb("sigcb", "sigcb", 2, 0, 11);
RooRealVar n("n","n", 1, 0.,  15); n.setConstant(true);
RooRealVar a("a","a", 2, 1., 10); //a.setConstant(true);
RooCBShape cb("cb","cb",m, meang,sigcb,a,n);

RooRealVar frac("frac", "frac", 0.6, 0, 1); //frac1.setConstant(true);
//RooRealVar frac("frac", "frac", 0.6, 0.6, 1); //frac1.setConstant(true);
RooAddPdf lcsum("lcsum", "lcsum",RooArgList(cb, gauss),frac);



// Total fit (signal+bkg)

RooAddPdf sum("sum", "sum",RooArgList(lcsum, bkg), RooArgList(nlc,  nback));
//RooAddPdf sum("sum", "sum",RooArgList(gauss, bkg), RooArgList(nlc,  nback));
RooFitResult* result =  sum.fitTo(data, Extended(), Save(), Optimize(false));

// RooStats::SPlot sData = new RooStats::SPlot("sData","An SPlot",
			     //  data, &sum, RooArgList(nlc, nback) );

//std::cout << "\nThe dataset before creating sWeights:\n";
data.Print();
//sum->paramOn(xframe, Format("NE",FixedPrecision(4)));

RooStats::SPlot sData ("sData","An SPlot",
			       data, &sum, RooArgList(nlc, nback) );

//RooStats::SPlot sData("sData","An SPlot",
			     //  data, sum, RooArgList(nlc, nback) );
//std::cout << "\n\nThe dataset after creating sWeights:\n";

data.Print();

m.setRange("signal",meang.getVal()-2*sigmag.getVal(),meang.getVal()+2*sigmag.getVal());
        RooAbsReal* fsigregion_sum = sum.createIntegral(m,NormSet(m),Range("signal")); //The "NormSet(x)" normalizes it to the total number of events to give you the fraction n_signal_region_events/n_total_events
        RooAbsReal* fsigregion_bkg = bkg.createIntegral(m,NormSet(m),Range("signal")); 



//Double_t nsigevents = fsigregion_sum->getVal()*(nlc.getVal()+nback.getVal())-fsigregion_bkg->getVal()*nback.getVal(); 
Double_t Syieldrange = fsigregion_sum->getVal()*(nlc.getVal()+nback.getVal())-fsigregion_bkg->getVal()*nback.getVal(); 
Double_t Byieldrange = fsigregion_bkg->getVal()*(nback.getVal()); 
//Double_t Byieldrange = fsigregion_bkg->getVal()*(nlc.getVal()+nback.getVal()); 
  //      Double_t fsig = nsigevents/(fsigregion_sum->getVal()*(nlc.getVal()+nback.getVal()));

//Double_t Syieldrange = fsigregion_sum->getVal()*; 
//Double_t Byieldrange = fsigregion_bkg->getVal(); 

std::cout << "\n\n Signal events "<<Syieldrange<<"\n";
std::cout << "\n\n BG events "<<Byieldrange<<"\n";

//f1->Close();
/*

//TFile *hfile = new TFile("LcStudy_sweights.root","RECREATE");
//TFile *hfile = new TFile("LcStudy_sweights_odd.root","RECREATE");
//TFile *hfile = new TFile("LcStudy_sweights_even.root","RECREATE");
//TFile *hfile = new TFile("LcStudy_sweights_even_newfit.root","RECREATE");
//TFile *hfile = new TFile("LcStudy_sweights_even_FitOct.root","RECREATE");
TFile *hfile = new TFile("LcStudy_sweights_odd_FitOct.root","RECREATE");
hfile->cd();

//TTree *treew = new TTree("SWeights","Weights_vars");
auto treew = t1->CloneTree(0);

treew->CopyEntries(t1);

//hfile->Write();
//data.convertToTreeStore()
//Double_t nw_back, nw_sig;

float sw_back, sw_sig;

TBranch* newb_back = treew->Branch("sw_back",&sw_back, "sw_back/F");
TBranch* newb_sig  = treew->Branch("sw_sig",&sw_sig, "sw_sig/F");
//auto newBranch = treew->Branch("nw_back", &nw_back, "nw_back/F");
//treew->Branch("nw_back",&nw_back);
//treew->Branch("nw_sig",&nw_sig);

//std::cout << "\n\nReading entries later:  "<<nentries<<std::endl;
//for (int i=0; i<10; i++){
for (int i=0; i<nentries;i++){
treew->GetEntry(i);
sw_sig = sData.GetSWeight(i, "nlc");
sw_back = sData.GetSWeight(i, "nback");
//h2->Fill(nw_sig);
//treew->Branch("nw_sig",&nw_sig);
//treew->Branch("nw_back",&nw_back);
//treew->Fill();
newb_back->Fill();
newb_sig->Fill();
 }

treew->Write();

hfile->Close();
//h2->Draw("");
//auto treeweights = t1->CloneTree(-1);
//treeweights->SetBranchAddress("nw_sig",&nw_sig);
//treeweights->SetBranchAddress("nw_back",&nw_back);
//treeweights->AddFriend("nw_sig");
//treeweights->AddFriend("nw_back");

//hfile->Close();
*/

// Check that our weights have the desired properties

std::cout << "Check SWeights:" << std::endl;

std::cout << std::endl <<  "Yield of Signal is "
<< nlc.getVal() << ".  From sWeights it is "
<< sData.GetYieldFromSWeight("nlc") << std::endl;

std::cout << std::endl <<  "Yield of Background is "
<< nback.getVal() << ".  From sWeights it is "
<< sData.GetYieldFromSWeight("nback") << std::endl;



gStyle->SetOptStat(111);

RooPlot * mframe = m.frame();
RooPlot *frame2 = m.frame();


data.plotOn(mframe);
data.statOn(mframe);
//gauss.plotOn(mframe);
//gauss.plotOn(mframe,Components(gauss), LineColor(1), LineStyle(2)) ;
//cb.plotOn(mframe);
//cb.plotOn(mframe,Components(cb), LineColor(5), LineStyle(2)) ;

sum.paramOn(mframe, Format("NE",FixedPrecision(4)));
//gauss.plotOn(mframe);
//bkg.plotOn(mframe);
sum.plotOn(mframe);
sum.plotOn(mframe,Components(lcsum), LineColor(3), LineStyle(2)) ;
  //sum.plotOn(mframe,Components(bkg),  LineColor(4),LineStyle(4)) ;
sum.plotOn(mframe,Components(bkg),  LineColor(2),LineStyle(4)) ;


TCanvas *c1_Data = new TCanvas("c1_Data", "", 800, 600);
c1_Data->Divide(1, 2);
c1_Data->cd(1);
gPad->Update();
//gPad-> SetLogy();
//mframe->SetMinimum(0.01);
gPad->SetTopMargin(0);
gPad->SetLeftMargin(0.15);
gPad->SetRightMargin(0.05);
gPad->SetPad(0.02, 0.02, 0.95, 0.77);
mframe->SetTitle("");
//mframe->SetMaximum(mframe->GetMaximum()*1.4);
mframe->Draw();

TLatex *myLatex = new TLatex(0.5,0.5,"");
myLatex->SetTextFont(132);
myLatex->SetTextColor(1);
myLatex->SetTextSize(0.05);
myLatex->SetNDC(kTRUE);
myLatex->SetTextAlign(11);


//myLatex->DrawLatex(0.65, 0.8,"LHCb Preliminary");
//mframe->GetXaxis()->SetTitle("m(p K^{-} #pi^{+}) [MeV/c^{2}]");
mframe->GetYaxis()->CenterTitle(true);
mframe->GetXaxis()->SetTitle("Lc_mass [MeV/c^{2}] ");
mframe->GetYaxis()->SetTitle("Counts / 1.1 [MeV/c^{2}]");

char Syield[30], Byield[30], fvalue[30];
sprintf(Syield, "S yield  %d", int(nlc.getVal()));
myLatex->DrawLatex(0.2, 0.9,"LHCb Preliminary");
myLatex->DrawLatex(0.2, 0.8,"Odd Event Numbers");
//myLatex->DrawLatex(0.2, 0.8,"Even Event Numbers");
sprintf(Byield, "B yield  %d", int(nback.getVal()));
myLatex->DrawLatex(0.2, 0.7,  Syield);
myLatex->DrawLatex(0.2, 0.65, Byield);
sprintf(fvalue, "f= %f", frac.getVal());
myLatex->DrawLatex(0.2, 0.6,  fvalue);



char Sregion[40], Bregion[40], range [30];
sprintf (Sregion,  "%d ", int(Syieldrange));
sprintf (Bregion,  "%d ", int(Byieldrange));
//sprintf (Bregion,  "Byield in #pm 2#sigma  %f ", Byieldrange);
myLatex->DrawLatex(0.2, 0.5,   "Syield in #pm 2#sigma");
myLatex->DrawLatex(0.2, 0.45,   Sregion);
myLatex->DrawLatex(0.2, 0.4,   "Byield in #pm 2#sigma");
myLatex->DrawLatex(0.2, 0.35,  Bregion);
sprintf (range,  "[%d, %d] ",  int(meang.getVal()-2*sigmag.getVal()), int(meang.getVal()+2*sigmag.getVal()));
myLatex->DrawLatex(0.2, 0.25,  range);

data.plotOn(frame2, Name("dataHist"),MarkerSize(0.8), DataError(RooAbsData::SumW2));
sum.plotOn(frame2, LineColor(kBlue));

c1_Data->cd(2);
gPad->SetTopMargin(0);
gPad->SetLeftMargin(0.15);
gPad->SetRightMargin(0.05);
gPad->SetPad(0.02, 0.76, 0.95, 0.97);
RooHist* hpull1   = frame2->pullHist();
RooPlot* mframeh1 = m.frame(Title(" "));
//RooPlot* mframeh1 = m.frame();
hpull1->SetFillColor(15);
hpull1->SetFillStyle(3144);
//mframeh1->addPlotable(hpull1, "L3");
mframeh1->addPlotable(hpull1, "p");
mframeh1->GetYaxis()->SetNdivisions(505);
mframeh1->GetYaxis()->SetLabelSize(0.15);
mframeh1->SetMinimum(-4.5);
mframeh1->SetMaximum(4.5);
mframeh1->Draw();



c1_Data->SaveAs("Lcfit_new_odd.pdf");
c1_Data->SaveAs("Lcfit_new_odd.root");



//file.close();

//c3->cd();*/

}
//c6->Write();
//rootfile->Close();


return;
}

