#include <string>
#include <sstream>
#include <iostream>
#include "TTree.h"
#include "TFile.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TDirectory.h"
//#include "TH2D.h"
//#include "TProfile.h"
//#include "RooGaussian.h"
//#include "RooDataHist.h"
//#include "RooPlot.h"
//#include "RooAddPdf.h"
//#include "TSpectrum.h"
//using namespace RooFit ;

void TMVAPunzi_allMC_new() {
    
   //TFile* f = TFile::Open("/eos/lhcb/user/f/feoliva/Lc2pKpi/TMVA_CB_EmmyVar.root");
   //TFile* f = TFile::Open("TMVA_class.root");
   //TFile* f = TFile::Open("TMVA_classification_LcSelection.root");
  // TFile* f = TFile::Open("TMVA_classification_allMCmasses.root");
   //TFile* f = TFile::Open("TMVA_classification_allMCmasses_Set1_LcKPiMC_Allmasses_WS1.root");
   //TFile* f = TFile::Open("TMVA_classification_allMCmasses_Set1_LcKPiMC_Allmasses_WS2.root");
   TFile* f = TFile::Open("TMVA_classification_allMCmasses_Set1_LcKPiMC_Allmasses_WS3.root");
   
   //TFile* f = TFile::Open("TMVA_classification.root");
  // 

   TCanvas *c1 = new TCanvas("c1", "c1", 700, 600);


 
   TH1F *histB = (TH1F*)f->Get("dataset_LcImpr_WS3/Method_BDT/BDTG/MVA_BDTG_effB");
   TH1F *histS = (TH1F*)f->Get("dataset_LcImpr_WS3/Method_BDT/BDTG/MVA_BDTG_effS");
   TH1F *histcorrS = (TH1F*)f->Get("dataset_LcImpr_WS3/CorrelationMatrixS");
   TH1F *histcorrB = (TH1F*)f->Get("dataset_LcImpr_WS3/CorrelationMatrixB");


    //TH1F *h1 = new TH1F("h1", "h1 title", 100, 0, 4.4);
    //TH1F *sigcurve = new TH1F("sigcurve","Significance VS TMVA response", 10000, -1, 1 );

    //double numB = 37129;
    //double numS = 75713;
    double LumiScaleFactor2016;
    //LumiScaleFactor2016= 1./0.29816; 
    LumiScaleFactor2016=1;    


    //from previous BG evaluation
    double bkg1, bkg2, bkg3, bkg4;
    

    //bkg1 =5228;
    //bkg2 =13895;
    //bkg3 =20687;
    //bkg4 =12483;


    //Background with Kbach cut
    //bkg1 =5006;
    //bkg2 =12852;
    //bkg3 =17764;
    //bkg4 =10390;

    //Background with Kbach cut and probNNk 0.3
    
    //bkg1=2725;
    //bkg2=7032;
    //bkg3=9745;
    //bkg4=5641;

    //Background with Kbach cut and probNNk 0.1
      
    // bkg1=4051;
    // bkg2=10420;
    // bkg3=14417; 
    // bkg4=8436;

     //Background with Kbach cut and probNNk 0.3 and Lcpi mass cut 2600
     //
      
     //bkg1=2723;
     //bkg2=7031;
     //bkg3=9603;
     //bkg4=5446;
  
    //Background with Kbach cut and probNNk 0.3 and Lcpi mass cut 3000
    //     
     //WS1

     /*bkg1=124151;
     bkg2=340579;
     bkg3=510486;
     bkg4=305040; */

     //WS2
     //
     /*bkg1=106866;
     bkg2=298209;
     bkg3=458171;
     bkg4=275082;*/

     //WS3
     bkg1=154329;
     bkg2=426850;
     bkg3=627744;
     bkg4=370695; 


    TCanvas* cS = new TCanvas("cS", "", 800, 400);
    histS->Draw();
    cS->SaveAs("effS.pdf");
    TCanvas* cB = new TCanvas("cB", "", 800, 400);
    histB->Draw();
    cB->SaveAs("effB.pdf");

    
    gStyle->SetPalette(kRainBow);
    TCanvas* corrS = new TCanvas("corrS", "", 800, 400);
    TCanvas* corrB = new TCanvas("corrB", "", 800, 400);
    //corrS->cd();
    //histcorrS->Draw("zcol text");
    //corrB->cd(); 
    //histcorrB->Draw("zcol text");

    //    double effS = 0;
    //    double effB = 0;

   //Punzi FOM for different MVA response cut 
   //

    double xmin = -1;
    double xmax =  1;
    double xvalue, cut1, cut2, cut3, cut4;

 
    ofstream fomoutput;
    fomoutput.open("FOMoutput.txt");


    fomoutput<<" Range \t Punzi FOM "<<std::endl;

    //Define step
    //
    //
    //
    //
    //
    double punzi1, punzi2, punzi3, punzi4;
    int    nstep = 10;
    //double step = 0.1;
    //double step = (xmax-xmin)/nstep;
    double step = 0.2;
    double binminS, binminB;
    double binmaxS, binmaxB;
    double max1, max2, max3, max4;
   // float x[nstep], y[nstep];
    float x[2000], y1[2000], y2[2000], y3[2000], y4[2000];;
   
    //binminS = histS->FindFixBin(xmin);
    //binminB = histB->FindFixBin(xmin);

     int j=0;
     max1=0; max2=0; max3=0; max4=0;

    //for (double i = xmin+step; i < xmax+step; i=i+step){
    //for (double i = 1; i < 10000; i=i+100){
    for (double i = 1; i < 100000; i=i+100){
      double effS = 0;
      double effB = 0;
      effS = histS->GetBinContent(i);
      effB = histB->GetBinContent(i);


      //binminS = histS->FindFixBin(xmin);
      //binmaxS = histS->FindFixBin(i);
      //binminB = histB->FindFixBin(xmin);
     // binmaxB = histB->FindFixBin(i);


      
      //binminS = histS->FindFixBin(i);
      //binmaxS = histS->FindFixBin(xmax);
      //binminB = histB->FindFixBin(i);
      //binmaxB = histB->FindFixBin(xmax);

      /*binminS = histS->GetBinCenter(xmin);
      binmaxS = histS->GetBinCenter(i);
      binminB = histB->GetBinCenter(xmin);
      binmaxB = histB->GetBinCenter(i);*/

 
      //effB = histB->Integral(binminB, binmaxB);
      //effS = histS->Integral(binminS, binmaxS);
     
     
       
      punzi1=effS/( 2.5 + sqrt(effB*bkg1 * LumiScaleFactor2016) );
      punzi2=effS/( 2.5 + sqrt(effB*bkg2 * LumiScaleFactor2016) );
      punzi3=effS/( 2.5 + sqrt(effB*bkg3 * LumiScaleFactor2016) );
      punzi4=effS/( 2.5 + sqrt(effB*bkg4 * LumiScaleFactor2016) );
 
  
      xvalue = histS->GetBinCenter(i);      

      x[j]=xvalue;
      y1[j]=punzi1;
      y2[j]=punzi2;
      y3[j]=punzi3;
      y4[j]=punzi4;
     
      if(max1<punzi1) {max1=punzi1; cut1=xvalue;}
      if(max2<punzi2) {max2=punzi2; cut2=xvalue;}
      if(max3<punzi3) {max3=punzi3; cut3=xvalue;}
      if(max4<punzi4) {max4=punzi4; cut4=xvalue;}

      //fomoutput<<i<<"\t"<<punzi<<endl;
      //  fomoutput<<"prova    "<<endl;
      

      j++;

      
      }


    fomoutput<<"Max Punzi value MC1 "<<max1<<endl;
    fomoutput<<"Max Punzi value MC2 "<<max2<<endl;
    fomoutput<<"Max Punzi value MC3 "<<max3<<endl;
    fomoutput<<"Max Punzi value MC4 "<<max4<<endl;

    fomoutput.close();
    

    c1->cd();  
    c1->SetGrid();

    TMultiGraph *multigraph = new TMultiGraph();
    //multigraph->SetName("");
    //multigraph->SetTitle("");
   
    TGraph* gr1 = new TGraph(j, x, y1);
    //TGraphErrors * gr = new TGraphErrors(nstep,x, y, dx, dy); 
   // gr1->SetLineColor(4);
    gr1->SetMarkerStyle(20);   
     
    gr1->SetLineWidth(2);
    gr1->SetLineColor(kGreen);
    gr1->SetTitle("Punzi FOM vs MVA Cut");

    gr1->GetXaxis()->SetLabelSize(0.02);
    gr1->GetYaxis()->SetLabelSize(0.02);
    gr1->GetXaxis()->SetTitleSize(0.03);
    gr1->GetYaxis()->SetTitleSize(0.03);
    gr1->GetYaxis()->SetTitleOffset(1.2);  
    gr1->GetXaxis()->SetRangeUser(-1, 1);  
    //gr->GetYaxis()->SetRangeUser(0, 1);  
    gr1->GetXaxis()->SetTitle("MVA Cut");
    gr1->GetYaxis()->SetTitle("Punzi FOM"); 
    double Max1=gr1->GetMaximum();
   

    multigraph->Add(gr1,"");
    //gr->Draw("AL");
    //gr->Draw("l");
    TGraph* gr2 = new TGraph(j, x, y2);
    gr2->SetLineWidth(2);
    gr2->SetLineColor(kRed);
    multigraph->Add(gr2,"");
    TGraph* gr3 = new TGraph(j, x, y3);
    gr3->SetLineWidth(2);
    gr3->SetLineColor(kBlue);
    multigraph->Add(gr3,"");
    TGraph* gr4 = new TGraph(j, x, y4);
    gr4->SetLineWidth(2);
    gr4->SetLineColor(kGreen+2);
    multigraph->Add(gr4,"");
  
        
    multigraph->Draw("AL");  
    multigraph->GetXaxis()->SetTitle("MVA Cut");
    multigraph->GetYaxis()->SetTitle("Punzi FOM");   
    multigraph->GetXaxis()->SetLabelSize(0.02);
    multigraph->GetYaxis()->SetLabelSize(0.02);
    multigraph->GetXaxis()->SetTitleSize(0.03);
    multigraph->GetYaxis()->SetTitleSize(0.03);
    multigraph->GetYaxis()->SetTitleOffset(1.2);
    multigraph->GetXaxis()->SetRangeUser(-1, 1);

   /* TLatex *myLatex = new TLatex(0.5,0.5,"");
    myLatex->SetTextFont(132);
    myLatex->SetTextColor(1);
    myLatex->SetTextSize(0.05);
    myLatex->SetNDC(kTRUE);
    myLatex->SetTextAlign(11); 
    char maxvalue[40], optcut [30];
    sprintf (maxvalue,  "%f ", Maximum);
    myLatex->DrawLatex(0.2, 0.5,   "Max Significance");
    myLatex->DrawLatex(0.4, 0.5,   maxvalue);   
    sprintf (optcut,  "%f ", cut);   
    myLatex->DrawLatex(0.2, 0.4,   "Optimal Cut");
    myLatex->DrawLatex(0.4, 0.4,   optcut);  

    */

    TLatex *myLatex = new TLatex(0.5,0.5,"");
    myLatex->SetTextFont(132);
    myLatex->SetTextColor(1);
    myLatex->SetTextSize(0.03);
    myLatex->SetNDC(kTRUE);
    myLatex->SetTextAlign(11);

    char  optcut1 [30], optcut2 [30], optcut3 [30], optcut4 [30];
    sprintf (optcut1,  "Opt cut MC 1 %f ", cut1);
    sprintf (optcut2,  "Opt cut MC 2 %f ", cut2);
    sprintf (optcut3,  "Opt cut MC 3 %f ", cut3);
    sprintf (optcut4,  "Opt cut MC 4 %f ", cut4);
    //TLegend* t = new TLegend(0.6353383,0.4982578,0.7994987,0.7003484);
    TLegend* t = new TLegend(0.15,0.55,0.49,0.9);
    t->AddEntry(gr1,"MC mass 2942 MeV","l");
    myLatex->DrawLatex(0.5, 0.85,   optcut1);
    t->AddEntry(gr2,"MC mass 2964.3 MeV","l");
    myLatex->DrawLatex(0.5, 0.8,   optcut2);
    t->AddEntry(gr3,"MC mass 3055.9 MeV","l");
    myLatex->DrawLatex(0.5, 0.75,   optcut3);
    myLatex->DrawLatex(0.5, 0.7,   optcut4);
    t->AddEntry(gr4,"MC mass 3077.2 MeV","l");
    t->SetTextAlign(12);
    t->SetBorderSize(1);
    t->SetFillStyle(0);
    t->SetFillColor(0);
    t->SetTextFont(43);
    t->SetTextSize(15);
    t->Draw();

    t->Draw();


}

