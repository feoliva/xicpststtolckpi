j = Job(name='First ganga job')
myApp = GaudiExec()
myApp.directory = "/afs/cern.ch/user/f/feoliva/AnalysisProductions/DaVinciDev_v45r1"
j.application = myApp
#j.application.options = ['Xic0StStToLcKPi.py']
#j.application.options = ['XicpStStToLcKPi_newcuts.py']
j.application.options = ['XicpStStToLcKPi_jobs_WSfixed.py']
j.application.platform = 'x86_64-centos7-gcc8-opt'
#bkPath = '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/27163002/ALLSTREAMS.DST'
#bkPath = '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo03a/94000000/CHARMSPECPARKED.MDST'
#j.inputdata = BKQuery('/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo03a/94000000/CHARMSPECPRESCALED.MDST').getDataset()
bkPath = '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo03a/94000000/CHARMSPECPRESCALED.MDST'
data  = BKQuery(bkPath, dqflag=['OK']).getDataset()
#j.inputdata = data[501:1000]
j.inputdata = data[0:1000]
j.backend = Dirac()
j.splitter = SplitByFiles(filesPerJob=5)
#j.outputfiles = [LocalFile('Xi0StStToLcKPi_var.root')]
j.outputfiles = [DiracFile('XicpStStToLcKPi_Lcjobs_WSfixed.root')]
#j.outputfiles = [DiracFile('XicpStStToLcKPi_NewCuts.root')]
j.submit()

