from GaudiConf import IOHelper
from StandardParticles import StdAllNoPIDsPions as pions
from StandardParticles import StdTightANNKaons as kaons

from Configurables import (
    DaVinci,
    DstConf,
    TurboConf,
    TupleToolRecoStats,
    CondDB,
    TupleToolMCTruth,
    TupleToolMCBackgroundInfo,
    TupleToolDecay,
    LoKi__Hybrid__DictOfFunctors,
    LoKi__Hybrid__Dict2Tuple,
    LoKi__Hybrid__DTFDict as DTFDict,
)
from PhysConf.Selections import (
    FilterSelection,
    MomentumScaling,
    MomentumSmear,
    TupleSelection,
    CombineSelection,
    Combine3BodySelection,
    SelectionSequence,
    RebuildSelection,
    AutomaticData,
)
from PhysConf.Filters import LoKi_Filters
from TeslaTools import TeslaTruthUtils
from DecayTreeTuple.Configuration import *  # noqa: F403, F401

# can be taken out for AProds
DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().Turbo = True
#


all_sequences = []

HLT2_LINE = "Hlt2CharmHadLcpToPpKmPipTurbo"

toolList = [
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolANNPID",
    "TupleToolGeometry",
    "TupleToolPrimaries",
    "TupleToolTrackInfo",
    "TupleToolEventInfo",
]

mcToolList = ["MCTupleToolKinematic", "MCTupleToolHierarchy"]


Lc_LoKiVariables = {
    "BPVVDCHI2": "BPVVDCHI2",
    "BPVIPCHI2": "BPVIPCHI2()",
    # DOCA information
    "DOCAMAX": "DOCAMAX",
    "DOCAMIN": "LoKi.Particles.PFunA(AMINDOCA('LoKi::TrgDistanceCalculator'))",
    "DOCACHI2MAX": "DOCACHI2MAX",
    "DOCA12": "DOCA(1,2)",
    "DOCA13": "DOCA(1,3)",
    "DOCA23": "DOCA(2,3)",
    "Y": "Y",
    "ETA": "ETA",
    "SUM_PT": "(CHILD(PT, 1) + CHILD(PT, 2) + CHILD(PT, 3))",
    "M12": "M12",
    "M23": "M23",
    "M13": "M13",
}


# Get the Lc, kaon, pion (persistreco) candidates
Lcp = AutomaticData("%s/Particles" % HLT2_LINE)

Lcp = MomentumSmear(Lcp)

# Lc cuts
#Lcp = FilterSelection("Lc2pKpi_filter", [Lcp], Code="(BPVIPCHI2() < 9)")


def LcaddLoki(dtt, branch_name):
    branch = getattr(dtt, branch_name)
    branch.addTupleTool(
        "LoKi::Hybrid::TupleTool/LoKi/%s" % branch_name
    ).Variables = dict(**Lc_LoKiVariables)


## Lambda_c tree

tupSel = TupleSelection(
    "Lc2PKPi",
    [Lcp],
    Decay="[Lambda_c+ -> ^p+  ^K- ^pi+]CC",
    Branches={
        "Lc": "^[Lambda_c+ -> p+  K- pi+]CC",
        "Lc_p": "[Lambda_c+ -> ^p+  K- pi+]CC",
        "Lc_Km": "[Lambda_c+ -> p+  ^K- pi+]CC",
        "Lc_pip": "[Lambda_c+ -> p+  K- ^pi+]CC",
    },
)

Lc_dtt = tupSel.algorithm()
Lc_dtt.ToolList = toolList[:]
Lc_dtt.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
Lc_dtt.TupleToolRecoStats.Verbose = True
Lc_dtt.addTool(TupleToolDecay)
LcaddLoki(Lc_dtt, "Lc")
#Lc_dtt.ToolList += ["TupleToolMCBackgroundInfo", "TupleToolMCTruth"]
#relations = TeslaTruthUtils.getRelLocs() + [
#            TeslaTruthUtils.getRelLoc(""),
            # Location of the truth tables for PersistReco objects
#            "Relations/Hlt2/Protos/Charged",
#        ]
#TeslaTruthUtils.makeTruth(Lc_dtt, relations, mcToolList)


all_sequences += [SelectionSequence("Lc2PKPi_seq", tupSel).sequence()]

transientEventStore_root = {
    "2016": "/Event/Turbo",
    "2017": "/Event/Charmspec/Turbo",
    "2018": "/Event/Charmspec/Turbo",
}

DaVinci().PrintFreq = 10000
DaVinci().RootInTES = transientEventStore_root[DaVinci().DataType]
DaVinci().InputType = "DST"

# Ignore events that don't pass the HLT2 trigger.
#trigger_filter = LoKi_Filters( 
    # Adjust this regular expression to match whatever set of lines you're
    # interested in studying
#    HLT2_Code="HLT_PASS_RE('.*{0}.*')".format(HLT2_LINE)
#)
#DaVinci().EventPreFilters = trigger_filter.filters("TriggerFilter")

# insert our sequence into DaVinci
DaVinci().UserAlgorithms = all_sequences
DaVinci().EvtMax =  -1
DaVinci().Lumi = not DaVinci().Simulation

#Sim9
#DaVinci().CondDBtag = 'sim-20161124-2-vc-md100'
#DaVinci().DDDBtag = 'dddb-20150724'
#Sim10
DaVinci().DDDBtag = 'dddb-20210528-6'
DaVinci().CondDBtag = 'sim-20201113-6-vc-md100-Sim10'


DaVinci().TupleFile  = 'Validation.root'

#IOHelper().inputFiles([('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00061410/0000/00061410_00000401_7.AllStreams.dst')], #Simulation9
IOHelper().inputFiles([('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.MDST/00138217/0000/00138217_00000061_7.AllStreams.mdst')],    #Simulation10
                      clear=True)
# =============================================================================
# Stuff specific to Persist Reco, not needed for "plain" Turbo
# =============================================================================
# Lines below prevents the error "WARNING Inconsistent setting of
# PersistReco for TurboConf"

DstConf().Turbo = True
TurboConf().PersistReco = True
TurboConf().DataType = DaVinci().DataType

# (9) specific for persist reco (2016)
if TurboConf().DataType == "2016":
    from Configurables import DataOnDemandSvc
    from Configurables import Gaudi__DataLink as Link

    dod = DataOnDemandSvc()
    for name, target, what in [
        (
            "LinkHlt2Tracks",
            "/Event/Turbo/Hlt2/TrackFitted/Long",
            "/Event/Hlt2/TrackFitted/Long",
        )
    ]:
        dod.AlgMap[target] = Link(name, Target=target, What=what, RootInTES="")
