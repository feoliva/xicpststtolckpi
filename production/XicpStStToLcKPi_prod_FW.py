
from StandardParticles import StdAllNoPIDsPions as pions
from StandardParticles import StdTightANNKaons as kaons

from Configurables import (
    CheckPV,
    DaVinci,
    DstConf,
    TurboConf,
    TupleToolRecoStats,
    TupleToolTISTOS,
    TupleToolPropertime,
    SubstitutePID,
    CondDB,
    TupleToolMCTruth,
    TupleToolMCBackgroundInfo,
    MCDecayTreeTuple,
    TupleToolDecay,
    LoKi__Hybrid__DictOfFunctors,
    LoKi__Hybrid__Dict2Tuple,
    LoKi__Hybrid__DTFDict as DTFDict,
)
from PhysConf.Selections import (
    FilterSelection,
    MomentumScaling,
    MomentumSmear,
    TupleSelection,
    CombineSelection,
    Combine3BodySelection,
    SelectionSequence,
    RebuildSelection,
    AutomaticData,
    Selection,
)
from PhysConf.Filters import LoKi_Filters
from TeslaTools import TeslaTruthUtils
from DecayTreeTuple.Configuration import *  # noqa: F403, F401

# can be taken out for AProds
DaVinci().DataType = "2016"
DaVinci().Simulation = False
DaVinci().Turbo = True
#


all_sequences = []

HLT2_LINE = "Hlt2CharmHadLcpToPpKmPipTurbo"

toolList = [
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolANNPID",
    "TupleToolGeometry",
    "TupleToolPrimaries",
    "TupleToolTrackInfo",
    "TupleToolEventInfo",
]

Xic_decay_products ={     
    'K-': '(BPVIPCHI2() < 9) & (TRGHOSTPROB<0.3) & (PROBNNk>0.1) & (PT>200)',
    'pi+': '(BPVIPCHI2() < 9) & (TRGHOSTPROB<0.3) & (PROBNNpi>0.1) & (PT>200)'
}

LcKpi_combi_cuts = {    
    Combination12Cut="(AM -AM1 < 1800)",
    DaughtersCuts=Xic_decay_products,
    MotherCut= """(M - M1 < 1600) & (VFASPF(VCHI2PDOF) < 20)""",
    ReFitPVs= True,
    CheckOverlapTool="LoKi::CheckOverlap"
}

LcTree_LoKiVariables = {
     "M_DTF_PV":"DTF_FUN( M, True )"
    ,"CHI2NDOF_DTF_PV":"DTF_CHI2NDOF(True)"
    ,"M12_DTF_Lc_PV":"DTF_FUN( M12, True, strings( 'Lambda_c+') )"
    ,"M13_DTF_Lc_PV":"DTF_FUN( M13, True, strings( 'Lambda_c+') )"
    ,"M23_DTF_Lc_PV":"DTF_FUN( M23, True, strings( 'Lambda_c+') )"
    ,"CHI2NDOF_DTF_Lc_PV":"DTF_CHI2NDOF(True, strings( 'Lambda_c+'))"
    ,"BPVVDCHI2":"BPVVDCHI2",
    "BPVIPCHI2": "BPVIPCHI2()",
    # DOCA information
    "DOCAMAX": "DOCAMAX",
    "DOCAMIN": "LoKi.Particles.PFunA(AMINDOCA('LoKi::TrgDistanceCalculator'))",
    "DOCACHI2MAX": "DOCACHI2MAX",
    "DOCA12": "DOCA(1,2)",
    "DOCA13": "DOCA(1,3)",
    "DOCA23": "DOCA(2,3)",
    "Y": "Y",
    "ETA": "ETA"
}


if not DaVinci().Simulation:
    CondDB(LatestGlobalTagByDataType=DaVinci().DataType)


# Get the Lc, kaon, pion (persistreco) candidates
Lcp = AutomaticData("%s/Particles" % HLT2_LINE)
my_kaons = RebuildSelection(kaons)
my_pions = RebuildSelection(pions)

if DaVinci().Simulation:
    Lcp = MomentumSmear(Lcp)
else:
    Lcp = MomentumScaling(Lcp, Turbo="PERSISTRECO", Year="2016")

# Lc cuts
Lcp = FilterSelection("Lc2pKpi", [Lcp], Code="(BPVIPCHI2() < 9)")


def MakeDTFD(
    dtt,
    branch_name,
    variables,
    tool_name="DTFTuple",
    constrainPV=True,
    constrainDaughters=None,
):
    branch = = getattr(dtt, branch_name)getattr(dtt, branch_name)

    DictTuple = branch.addTupleTool(LoKi__Hybrid__Dict2Tuple, tool_name)
    DictTuple.addTool(DTFDict, tool_name)
    DictTuple.Source = "LoKi::Hybrid::DTFDict/" + tool_name
    DictTuple.NumVar = len(
        variables.items()
    )  # reserve a suitable size for the dictionary

    DTF = getattr(DictTuple, tool_name)

    # configure the DecayTreeFitter in the usual way
    DTF.constrainToOriginVertex = constrainPV
    if constrainDaughters is not None:
        DTF.daughtersToConstrain = constrainDaughters

    # Add LoKiFunctors to the tool chain, just as we did to the Hybrid::TupleTool above
    # these functors will be applied to the refitted(!) decay tree
    # they act as a source to the DTFDict
    DTF.addTool(LoKi__Hybrid__DictOfFunctors, "dict_" + tool_name)
    DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict_" + tool_name

    DTF_dict = getattr(DTF, "dict_" + tool_name)
    DTF_dict.Variables = dict(**variables)

def XicaddLoki(dtt, branch_name):
    branch = getattr(dtt, branch_name)
    branch.addTupleTool(
        "LoKi::Hybrid::TupleTool/LoKi/%s" % branch_name
    ).Variables = dict(**Xic_LoKiVariables)


def make_tree(
    tree_name,
    input_particles,
    descriptor,
    decay,
    branches,
    combi_cuts,
    dtfd_branch_name=None,
    DTFDictVariables=None,
):  # Lc, K, pi

    combinationSel = Combine3BodySelection(
        tree_name + "_combination",
        input_particles,
        DecayDescriptor=descriptor,
        **combi_cuts
    )

    tupSel = TupleSelection(tree_name, combinationSel, Decay=decay, Branches=branches)

    dtt = tupSel.algorithm()

    dtt.ToolList = toolList[:]

    dtt.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    dtt.TupleToolRecoStats.Verbose = True
    dtt.addTool(TupleToolDecay)

    if dtfd_branch_name:
        MakeDTFD(
            dtt,
            dtfd_branch_name,
            DTFDictVariables,
            tool_name="DTF_PV_LcPDG",
            constrainPV=True,
            constrainDaughters=["Lambda_c+"],
        )

        MakeDTFD(
            dtt,
            dtfd_branch_name,
            DTFDictVariables,
            tool_name="DTF_PV",
            constrainPV=True,
        )

        MakeDTFD(
            dtt,
            dtfd_branch_name,
            DTFDictVariables,
            tool_name="DTF_LcPDG",
            constrainPV=False,
            constrainDaughters=["Lambda_c+"],
        )

    XicaddLoki(dtt, branch_name)   
    return SelectionSequence(tree_name + "_seq", tupSel).sequence(), dtt


## Lambda_c tree

tupSel = TupleSelection(
    "Lc",
    Lcp,
    Decay="^[Lambda_c+ -> ^p+  ^K- ^pi+]CC",
    Branches={
        "Lc": "[Sigma_c*+ -> ^(Lambda_c+ -> p+  K- pi+) K- pi+]CC",
        "p": "[Sigma_c*+ -> (Lambda_c+ -> ^p+  K- pi+) K- pi+]CC",
        "Km": "[Sigma_c*+ -> (Lambda_c+ -> p+  ^K- pi+) K- pi+]CC",
        "pip": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- ^pi+) K- pi+]CC",
    },
)


Lc_dtt = tupSel.algorithm()
Lc_dtt.addTupleTool("LoKi::Hybrid::TupleTool/LoKi/LokiToolLc").Variables = dict(**Lc_LoKiVariables)
Lc_seq = SelectionSequence("Lc_seq", tupSel).sequence()


# # right sign tree
LcKpi_RS = make_tree(
    "XicStSt_LcKpi",
    [Lcp, my_kaons, my_pions],
    "[Sigma_c*+ ->  Lambda_c+ K- pi+]cc",
    "[Sigma_c*+ ->  ^(Lambda_c+ -> ^p+  ^K- ^pi+) ^K- ^pi+]CC",
    {
        "Xicst": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) K- pi+]CC",
        "Lc": "[Sigma_c*+ -> ^(Lambda_c+ -> p+  K- pi+) K- pi+]CC",
        "p": "[Sigma_c*+ -> (Lambda_c+ -> ^p+  K- pi+) K- pi+]CC",
        "Km": "[Sigma_c*+ -> (Lambda_c+ -> p+  ^K- pi+) K- pi+]CC",
        "pip": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- ^pi+) K- pi+]CC",
        "Kbach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) ^K- pi+]CC",
        "pibach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) K- ^pi+]CC",
    },
    LcKpi_combi_cuts,
    dtfd_branch_name="Xicst",
    DTFDictVariables={
        "M12": "M12",
        "M": "M",
        "PT": "PT",
        "Lc_M": "CHILD(M, 1)",
        "Lc_PT": "CHILD(PT, 1)",
    },
)

## configure decaytreetuple here (LcKpi_RS)

## e.g. add Tools, LoKi functorsa..

all_sequences.append(LcKpi_RS_seq)


LcKpi_WS1 = make_tree(
    "XicStSt_LcKpiWS1",
    [Lcp, my_kaons, my_pions],
    "[Sigma_c*+ ->  Lambda_c+ K+ pi-]cc",
    "[Sigma_c*+ ->  ^(Lambda_c+ -> ^p+  ^K- ^pi+) ^K+ ^pi-]CC",
    {
        "Xicst": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) K+ pi-]CC",
        "Lc": "[Sigma_c*+ -> ^(Lambda_c+ -> p+  K- pi+) K+ pi-]CC",
        "p": "[Sigma_c*+ -> (Lambda_c+ -> ^p+  K- pi+) K+ pi-]CC",
        "Km": "[Sigma_c*+ -> (Lambda_c+ -> p+  ^K- pi+) K+ pi-]CC",
        "pip": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- ^pi+) K+ pi-]CC",
        "Kbach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) ^K+ pi-]CC",
        "pibach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) K+ ^pi-]CC",
    },
    LcKpi_combi_cuts,
    dtfd_branch_name="Xicst",
    DTFDictVariables={
        "M12": "M12",
        "M": "M",
        "PT": "PT",
        "Lc_M": "CHILD(M, 1)",
        "Lc_PT": "CHILD(PT, 1)",
    },
)


all_sequences.append(LcKpi_WS1_seq)

LcKpi_WS2 = make_tree(
    "XicStSt_LcKpiWS2",
    [Lcp, my_kaons, my_pions],
    "[Sigma_c*+ ->  Lambda_c+ K+ pi+]cc",
    "[Sigma_c*+ ->  ^(Lambda_c+ -> ^p+  ^K- ^pi+) ^K+ ^pi+]CC",
    {
        "Xicst": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) K+ pi+]CC",
        "Lc": "[Sigma_c*+ -> ^(Lambda_c+ -> p+  K- pi+) K+ pi+]CC",
        "p": "[Sigma_c*+ -> (Lambda_c+ -> ^p+  K- pi+) K+ pi+]CC",
        "Km": "[Sigma_c*+ -> (Lambda_c+ -> p+  ^K- pi+) K+ pi+]CC",
        "pip": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- ^pi+) K+ pi+]CC",
        "Kbach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) ^K+ pi+]CC",
        "pibach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) K+ ^pi+]CC",
    },
    LcKpi_combi_cuts,
    dtfd_branch_name="Xicst",
    DTFDictVariables={
        "M12": "M12",
        "M": "M",
        "PT": "PT",
        "Lc_M": "CHILD(M, 1)",
        "Lc_PT": "CHILD(PT, 1)",
    },
)

all_sequences.append(LcKpi_WS2_seq)

LcKpi_WS3 = make_tree(
    "XicStSt_LcKpiWS2",
    [Lcp, my_kaons, my_pions],
    "[Sigma_c*+ ->  Lambda_c+ K- pi-]cc",
    "[Sigma_c*+ ->  ^(Lambda_c+ -> ^p+  ^K- ^pi+) ^K- ^pi-]CC",
    {
        "Xicst": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) K- pi-]CC",
        "Lc": "[Sigma_c*+ -> ^(Lambda_c+ -> p+  K- pi+) K- pi-]CC",
        "p": "[Sigma_c*+ -> (Lambda_c+ -> ^p+  K- pi+) K- pi-]CC",
        "Km": "[Sigma_c*+ -> (Lambda_c+ -> p+  ^K- pi+) K- pi-]CC",
        "pip": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- ^pi+) K- pi-]CC",
        "Kbach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) ^K- pi-]CC",
        "pibach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) K- ^pi-]CC",
    },
    LcKpi_combi_cuts,
    dtfd_branch_name="Xicst",
    DTFDictVariables={
        "M12": "M12",
        "M": "M",
        "PT": "PT",
        "Lc_M": "CHILD(M, 1)",
        "Lc_PT": "CHILD(PT, 1)",
    },
)

all_sequences.append(LcKpi_WS3_seq)

###


transientEventStore_root = {
    "2015": "/Event/Turbo",
    "2016": "/Event/Turbo",
    "2017": "/Event/Charmmultibody/Turbo",
    "2018": "/Event/Charmmultibody/Turbo",
}

DaVinci().PrintFreq = 10000
DaVinci().RootInTES = transientEventStore_root[DaVinci().DataType]
DaVinci().InputType = "MDST"
DaVinci().TupleFile  = 'XicpStStToLcKPi.root', 
# Ignore events that don't pass the HLT2 trigger.
trigger_filter = LoKi_Filters(
    # Adjust this regular expression to match whatever set of lines you're
    # interested in studying
    HLT2_Code="HLT_PASS_RE('.*{0}.*')".format(HLT2_LINE)
)
DaVinci().EventPreFilters = trigger_filter.filters("TriggerFilter")

# (8) insert our sequence into DaVinci
DaVinci().UserAlgorithms = all_sequences

DaVinci().Lumi = not DaVinci().Simulation


IOHelper().inputFiles([('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMSPECPRESCALED.MDST/00076441/0000/00076441_00008679_1.charmspecprescaled.mdst')],
#IOHelper().inputFiles([('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/XICPTOPPKMPIP.HLTFILTER.MDST/00119297/0000/00119297_00000007_1.xicptoppkmpip.hltfilter.mdst')],
                      clear=True)


# =============================================================================
# Stuff specific to Persist Reco, not needed for "plain" Turbo
# =============================================================================
# Lines below prevents the error "WARNING Inconsistent setting of
# PersistReco for TurboConf"

DstConf().Turbo = True
TurboConf().PersistReco = True
TurboConf().DataType = DaVinci().DataType

# (9) specific for persist reco (2016)
if TurboConf().DataType == "2016":
    from Configurables import DataOnDemandSvc
    from Configurables import Gaudi__DataLink as Link

    dod = DataOnDemandSvc()
    for name, target, what in [
        (
            "LinkHlt2Tracks",
            "/Event/Turbo/Hlt2/TrackFitted/Long",
            "/Event/Hlt2/TrackFitted/Long",
        )
    ]:
        dod.AlgMap[target] = Link(name, Target=target, What=what, RootInTES="")

