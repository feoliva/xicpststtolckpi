from GaudiConf import IOHelper
from Configurables import (
    DaVinci,
    DstConf,
    TurboConf,
    TupleToolRecoStats,
    TupleToolTISTOS,
    TupleToolPropertime,
    SubstitutePID,
    CondDB,
    LoKi__Hybrid__DictOfFunctors,
    LoKi__Hybrid__Dict2Tuple,
    DecayTreeTuple,
)
from PhysConf.Selections import (
    FilterSelection,
    # MergedSelection,
    MomentumScaling,
    TupleSelection,
    CombineSelection,
    SelectionSequence,
    RebuildSelection,
    PrescaleSelection,
    AutomaticData,
    Selection,
)
from PhysConf.Filters import LoKi_Filters
from TeslaTools import TeslaTruthUtils
from DecayTreeTuple.Configuration import * 



CondDB(LatestGlobalTagByDataType="2016")



the_year = '2016'

if ((the_year == '2017') | (the_year == '2018')):
##    rootInTES = '/Event/Charmmultibody/Turbo'
    rootInTES = '/Event/Charmspec/Turbo'
if ((the_year == '2015') | (the_year == '2016')):
    rootInTES = '/Event/Turbo'



# DaVinci configuration
#rootInTES = '/Event/Charmspec/Turbo'

the_line  = 'Hlt2CharmHadLcpToPpKmPipTurbo/Particles' 


from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
        VOID_Code = """
            0 < CONTAINS('%s/%s')
                """ % ( rootInTES , the_line )
            )

Lcp = AutomaticData(the_line)
#Load input particles


from PhysConf.Selections import RebuildSelection

from StandardParticles import StdTightANNKaons as kaons
#from StandardParticles import StdAllLooseKaons as kaons
kaons = RebuildSelection (kaons)

from StandardParticles import StdTightANNPions as pions
#from StandardParticles import StdAllNoPIDsPions as pions
pions = RebuildSelection (pions)


Lcpscaled=MomentumScaling(Lcp, Turbo="PERSISTRECO", Year="2016")

#Selection LcpToPpKmPip


from PhysConf.Selections import FilterSelection
Lc2pKpi =  FilterSelection (
    'Lc2pKpi'              ,
    [ Lcpscaled ]      ,
    Code = "(BPVIPCHI2() < 9)"
    #Code = """(M > 2220) & (M < 2360)"""
    
    )

#Lc2pKpiPrescaled = PrescaleSelection ( Lc2pKpi , prescale = 0.2 )
Lc2pKpiPrescaled = PrescaleSelection ( Lc2pKpi , prescale = 1. )


Xicst2LcKpi = CombineSelection (
    'Xicst2LcKpi'       , 
    [ Lc2pKpi , kaons, pions ] , 
    #[ Lcpscaled , kaons, pions ] , 
    DecayDescriptors = ["[Sigma_c*+ ->  Lambda_c+ K- pi+]cc", "[Sigma_c*+ -> Lambda_c+ K+ pi-]cc", "[Sigma_c*+ -> Lambda_c+ K+ pi+]cc"],
    CombinationCut  = "(AM -AM1 < 1300)", 
    MotherCut = """
    (M - M1 < 1100) & 
    (VFASPF(VCHI2PDOF) < 20) 
    """,
    InputPrimaryVertices = 'Primary',
    CheckOverlapTool = "LoKi::CheckOverlap",
    ReFitPVs        = "True",
    DaughtersCuts = {
        "K+": "(BPVIPCHI2() < 9) & (TRGHOSTPROB<0.3) & (PROBNNk>0.1) & (PT>200)", 
        "pi+": "(BPVIPCHI2() < 9) & (TRGHOSTPROB<0.3) & (PROBNNpi>0.1) & (PT>200)"
    }
    )

####  Lc2pKpiTree###

from Configurables import DecayTreeTuple, TupleToolPrimaries
Lc2pKpiTree = DecayTreeTuple("Lc2pKpiTree")
Lc2pKpiTree.Inputs = [Lc2pKpi.outputLocation()]
#Lc2pKpiTree.Inputs = [Lcpscaled.outputLocation()]
Lc2pKpiTree.Decay = "[Lambda_c+ -> ^p+  ^K- ^pi+]CC"
Lc2pKpiTree.ToolList = [
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolANNPID",
    "TupleToolPropertime",
    "TupleToolGeometry",
    "TupleToolPrimaries",
    "TupleToolTrackInfo",
    "TupleToolEventInfo",
    "TupleToolRecoStats"
    #"TupleToolMCBackgroundInfo"
    #"TupleToolMCTruth"
     ]
Lc2pKpiTree.Branches = {
    "Lc"    :"[Lambda_c+ -> p+  K- pi+]CC",
    "p"     :"[Lambda_c+ -> ^p+  K- pi+]CC",
    "Km"    :"[Lambda_c+ -> p+  ^K- pi+]CC",
    "pip"   :"[Lambda_c+ -> p+  K- ^pi+]CC",
    }

#######

from Configurables import TupleToolRecoStats
Lc2pKpiTree.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
Lc2pKpiTree.TupleToolRecoStats.Verbose = True
##################################################
from Configurables import TupleToolDecay
Lc2pKpiTree.addTool(TupleToolDecay, name = 'Lc')
from Configurables import LoKi__Hybrid__TupleTool
Lc2pKpiTree.Lc.ToolList =  ["LoKi::Hybrid::TupleTool/LoKi_All0"]
###################################################
LoKiTuple0 = LoKi__Hybrid__TupleTool("LoKi_All0")
LoKiTuple0.Variables =  {
    "M_DTF_PV":"DTF_FUN( M, True )"
    ,"CHI2NDOF_DTF_PV":"DTF_CHI2NDOF(True)"
    ,"M12_DTF_Lc_PV":"DTF_FUN( M12, True, strings( 'Lambda_c+') )"
    ,"M13_DTF_Lc_PV":"DTF_FUN( M13, True, strings( 'Lambda_c+') )"
    ,"M23_DTF_Lc_PV":"DTF_FUN( M23, True, strings( 'Lambda_c+') )"
    ,"CHI2NDOF_DTF_Lc_PV":"DTF_CHI2NDOF(True, strings( 'Lambda_c+'))"
    ,"BPVVDCHI2":"BPVVDCHI2",
    # IPChi2 on the related PV.
    #"BPVIPCHI2": "BPVIPCHI2()",
    # DOCA information
    "DOCAMAX": "DOCAMAX",
    "DOCAMIN": "LoKi.Particles.PFunA(AMINDOCA('LoKi::TrgDistanceCalculator'))",
    "DOCACHI2MAX": "DOCACHI2MAX",
    "DOCA12": "DOCA(1,2)",
    "DOCA13": "DOCA(1,3)",
    "DOCA23": "DOCA(2,3)",
    # "M12": "M12",
    # "M13": "M13",
    # "M23": "M23",
    # Rapidity and pseudorapidity on the Xic+.
    "Y": "Y",
    "ETA": "ETA"
    }
Lc2pKpiTree.Lc.addTool(LoKiTuple0)

Lc2pKpiTree_tuple_sel = Selection("Lc2PKPiTree",
                                  Algorithm = Lc2pKpiTree,
                                  #RequiredSelections = [Lc2pKpiPrescaled])
                                  #RequiredSelections = [Lcpscaled])
                                   RequiredSelections = [Lc2pKpi])

####  LcKPi tree###

LcKpiTree = Lc2pKpiTree.clone("LcKpiTree")
LcKpiTree.Inputs = [Xicst2LcKpi.outputLocation()]
LcKpiTree.Decay = "[Sigma_c*+ ->  ^(Lambda_c+ -> ^p+  ^K- ^pi+) ^K- ^pi+]CC"
###################################################
LcKpiTree.Branches = {
    "Xicst" :"[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) [K-]CC [pi+]CC]CC",
    "Lc"    :"[Sigma_c*+ -> ^(Lambda_c+ -> p+  K- pi+) [K-]CC [pi+]CC]CC",
    "p"     :"[Sigma_c*+ -> (Lambda_c+ -> ^p+  K- pi+) [K-]CC [pi+]CC]CC",
    "Km"    :"[Sigma_c*+ -> (Lambda_c+ -> p+  ^K- pi+) [K-]CC [pi+]CC]CC",
    "pip"   :"[Sigma_c*+ -> (Lambda_c+ -> p+  K- ^pi+) [K-]CC [pi+]CC]CC",
    "Kbach":"[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) ^[K-]CC [pi+]CC]CC",
    "pibach":"[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) [K-]CC ^[pi+]CC]CC"
    }
LcKpiTree.addTool(TupleToolDecay, name = 'Xicst')

LcKpiTree.Xicst.ToolList +=  ["LoKi::Hybrid::TupleTool/LoKi_All3"]
###################################################
LoKiTuple3 = LoKi__Hybrid__TupleTool("LoKi_All3")
LoKiTuple3.Variables =  {
     
    "CHI2NDOF_DTF_Lc_PV":"DTF_CHI2NDOF(True, strings( 'Lambda_c+'))",
    "M_DTF_Lc":"DTF_FUN( M, False, strings( 'Lambda_c+'))",
    "CHI2NDOF_DTF_Lc":"DTF_CHI2NDOF(False, strings( 'Lambda_c+'))",
    "M_DTF_PV":"DTF_FUN( M, True)",
    "CHI2NDOF_DTF_PV":"DTF_CHI2NDOF(True)",
    "Dau3_M_DTF_Lc_PV":"DTF_FUN( CHILD(M,3), True , strings( 'Lambda_c+'))",
    "Dau3_PX_DTF_Lc_PV":"DTF_FUN( CHILD(PX,3), True , strings( 'Lambda_c+'))",
    "Dau3_PY_DTF_Lc_PV":"DTF_FUN( CHILD(PY,3), True , strings( 'Lambda_c+'))",
    "Dau3_PZ_DTF_Lc_PV":"DTF_FUN( CHILD(PZ,3), True , strings( 'Lambda_c+'))",
    "Dau3_PE_DTF_Lc_PV":"DTF_FUN( CHILD(E,3), True , strings( 'Lambda_c+'))",
    "Dau3_PT_DTF_Lc_PV":"DTF_FUN( CHILD(PT,3), True , strings( 'Lambda_c+'))",
    "Dau3_P_DTF_Lc_PV":"DTF_FUN( CHILD(P,3), True , strings( 'Lambda_c+'))",
    }

LcKpiTree.Xicst.addTool(LoKiTuple3)
LcKpiTree_tuple_sel = Selection("LcKPiTree",
                              Algorithm = LcKpiTree,
                              RequiredSelections = [ Xicst2LcKpi ])


####  LcKPi tree WS 1  LcKpPip###

LcKpiTreeWS1 = LcKpiTree.clone("LcKpiTreeWS1")
LcKpiTreeWS1.Inputs = [Xicst2LcKpi.outputLocation()]
LcKpiTreeWS1.Decay = "[Sigma_c*+ ->  ^(Lambda_c+ -> ^p+  ^K- ^pi+) ^K+ ^pi-]CC"
#LcKpiTreeWS1.addTool(TupleToolDecay, name = 'Xicst')
LcKpiTreeWS1_tuple_sel = Selection("LcKPiTreeWS1",
                              Algorithm = LcKpiTreeWS1,
                              RequiredSelections = [ Xicst2LcKpi ])
###################################################


####  LcKPi tree WS 2  LcKpPip###

LcKpiTreeWS2 = LcKpiTree.clone("LcKpiTreeWS2")
LcKpiTreeWS2.Inputs = [Xicst2LcKpi.outputLocation()]
LcKpiTreeWS2.Decay = "[Sigma_c*+ ->  ^(Lambda_c+ -> ^p+  ^K- ^pi+) ^K+ ^pi+]CC"
#LcKpiTreeWS2.addTool(TupleToolDecay, name = 'Xicst')
LcKpiTreeWS2_tuple_sel = Selection("LcKPiTreeWS2",
                              Algorithm = LcKpiTreeWS2,
                              RequiredSelections = [ Xicst2LcKpi ])
###################################################



from Configurables import DaVinci 
dv = DaVinci(
    PrintFreq  = 1000          ,
    DataType   = '2016'     ,
    InputType  = 'MDST'       ,  
    RootInTES  = rootInTES    ,  
    ##
    EventPreFilters = fltrs.filters('FILTER') , 
    Lumi       = True           , 
    Simulation = False          ,
    ## 
    #TupleFile  = 'Xi0StStToLcKPi_MassCut.root'   ,
    #TupleFile  = 'Xi0StStToLcKPi_TryProd.root'   ,
    #TupleFile  = 'XicpStStToLcKPi_NewCuts.root'   ,  
    TupleFile  = 'XicpStStToLcKPi_Lcjobs_WSfixed.root'   ,  
    EvtMax     =   -1,
    Turbo      = True      
 )

from PhysConf.Selections import SelectionSequence
sel1 = SelectionSequence('SEL1', Lc2pKpiTree_tuple_sel )
sel2 = SelectionSequence('SEL2', LcKpiTree_tuple_sel )
sel3 = SelectionSequence('SEL3', LcKpiTreeWS1_tuple_sel )
sel4 = SelectionSequence('SEL4', LcKpiTreeWS2_tuple_sel )

dv.UserAlgorithms = [ sel1.sequence(), sel2.sequence(), sel3.sequence(), sel4.sequence() ]

# Create an ntuple
#dtt = DecayTreeTuple('TupleXic0StSt')
#dtt.Inputs = Xic0StSt_seq.outputLocations()
#dtt.Decay = '[Sigma_c*+ -> Xi_c+ pi+ pi-]CC'

#DaVinci().UserAlgorithms += [Xic0StSt_seq.sequence(), dtt]


#IOHelper().inputFiles([('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMSPECPRESCALED.MDST/00076441/0000/00076441_00008679_1.charmspecprescaled.mdst')],
#IOHelper().inputFiles([('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/XICPTOPPKMPIP.HLTFILTER.MDST/00119297/0000/00119297_00000007_1.xicptoppkmpip.hltfilter.mdst')],
#                      clear=True)

# =============================================================================
# Stuff specific to Persist Reco, not needed for "plain" Turbo
# =============================================================================
# Lines below prevents the error "WARNING Inconsistent setting of
# PersistReco for TurboConf"

DstConf().Turbo = True
TurboConf().PersistReco = True
TurboConf().DataType = DaVinci().DataType

# (9) specific for persist reco (2016)
if TurboConf().DataType == "2016":
 from Configurables import DataOnDemandSvc
 from Configurables import Gaudi__DataLink as Link

 dod = DataOnDemandSvc()
for name, target, what in [
        (
            "LinkHlt2Tracks",
            "/Event/Turbo/Hlt2/TrackFitted/Long",
            "/Event/Hlt2/TrackFitted/Long",
        )
    ]:
 dod.AlgMap[target] = Link(name, Target=target, What=what, RootInTES="")


