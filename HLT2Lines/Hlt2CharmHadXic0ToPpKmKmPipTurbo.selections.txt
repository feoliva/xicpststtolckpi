
Hlt2CharmHadXic0ToPpKmKmPipTurboXic0ToPpKmKmPipTurboFilter
==========================================================

Selection
---------
Code: in_range( 2396.0 , M , 2770.0 )

Inputs
------

    Hlt2CharmHadXic0ToPpKmKmPipTurbo_DetachedHHHHCombinerTisTosTagger
    =================================================================

    Selection
    ---------
    TisTosSpecs:  { 'Hlt1.*Track.*Decision%TOS' : 0 }

    Inputs
    ------

        Hlt2CharmHadXic0ToPpKmKmPipTurbo_DetachedHHHHCombinerCombiner
        =============================================================

        Selection
        ---------
        DecayDescriptors: [ '[Xi_c0 -> p+ K- K- pi+]cc' ]
        Preambulo:        ['import math', 'lcldira = math.cos( 0.01 )']
        DaughtersCuts:    { '' : 'ALL' , 'K+' : '(PT > 500.0) & (MIPCHI2DV(PRIMARY) > 4.0)' , 'K-' : '(PT > 500.0) & (MIPCHI2DV(PRIMARY) > 4.0)' , 'p+' : '(PT > 500.0) & (MIPCHI2DV(PRIMARY) > 4.0)' , 'pi+' : '(PT > 500.0) & (MIPCHI2DV(PRIMARY) > 4.0)' , 'pi-' : '(PT > 500.0) & (MIPCHI2DV(PRIMARY) > 4.0)' , 'p~-' : '(PT > 500.0) & (MIPCHI2DV(PRIMARY) > 4.0)' }
        CombinationCut:   (in_range( 2386.0, AM, 2780.0 )) & ((APT1+APT2+APT3+APT4) > 3000.0 ) & (AHASCHILD(PT > 1000.0)) & (ANUM(PT > 500.0) >= 2) & (AHASCHILD((MIPCHI2DV(PRIMARY)) > 8.0)) & (ANUM(MIPCHI2DV(PRIMARY) > 6.0) >= 2)
        MotherCut:        (VFASPF(VCHI2PDOF) < 10.0) & (BPVDIRA > lcldira ) & (BPVVDCHI2 > 10.0 ) & (BPVLTIME() > 0.0001 )

        Inputs
        ------

            Hlt2CharmHadSharedPromptChildTighterProtonFilter
            ================================================

            Selection
            ---------
            Code: (PIDp > 5.0  ) & ( (PIDp-PIDK) > 5.0 ) & ( P > 10000.0 )

            Inputs
            ------

                Hlt2CharmHadSharedPromptChild_pFilter
                =====================================

                Selection
                ---------
                Code:   (TRCHI2DOF < 3.0 )& (TRGHOSTPROB < 0.4)& (PT > 500.0)& (P > 1000.0)& (PIDp > 10)

                Inputs
                ------

                    Hlt2BiKalmanFittedRichProtons
                    =============================

                    Selection
                    ---------
                    Code: (PPCUT(PP_HASINFO(LHCb.ProtoParticle.RichPIDStatus)))

                    Inputs
                    ------
                        Hlt2/Hlt2BiKalmanFittedProtons/Particles

            Hlt2CharmHadSharedPromptChild_KFilter
            =====================================

            Selection
            ---------
            Code:   (TRCHI2DOF < 3.0 )& (TRGHOSTPROB < 0.4)& (PT > 500.0)& (P > 1000.0)& (PIDK > 10)

            Inputs
            ------

                Hlt2BiKalmanFittedRichKaons
                ===========================

                Selection
                ---------
                Code: (PPCUT(PP_HASINFO(LHCb.ProtoParticle.RichPIDStatus)))

                Inputs
                ------
                    Hlt2/Hlt2BiKalmanFittedKaons/Particles

            Hlt2CharmHadSharedPromptChild_piFilter
            ======================================

            Selection
            ---------
            Code:   (TRCHI2DOF < 3.0 )& (TRGHOSTPROB < 0.4)& (PT > 500.0)& (P > 1000.0)& (PIDK < 0)

            Inputs
            ------

                Hlt2BiKalmanFittedRichPions
                ===========================

                Selection
                ---------
                Code: (PPCUT(PP_HASINFO(LHCb.ProtoParticle.RichPIDStatus)))

                Inputs
                ------
                    Hlt2/Hlt2BiKalmanFittedPions/Particles
