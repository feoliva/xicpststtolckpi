j = Job(name='Xic0 Eff HLT1 HLT2')
myApp = GaudiExec()
#myApp.directory = '/afs/cern.ch/work/f/feoliva/public/MooreAnalysisDev_master/'
myApp.directory = '/afs/cern.ch/work/f/feoliva/private/TestMooreDV/MooreAnalysisDev_master'
j.application = myApp
j.application.options = ['/afs/cern.ch/work/f/feoliva/private/TestMooreDV/hlt1_and_hlt2_eff_Xic0_new_ganga.py']
#j.application.options = ['/afs/cern.ch/work/f/feoliva/public/stack/MooreAnalysis/HltEfficiencyChecker/options/hlt2_eff_Lc2pKpi_ganga.py']
#j.application.platform = 'x86_64_v2-centos7-gcc11-dbg'
j.application.platform = 'x86_64_v2-centos7-gcc11-opt'
#bkPath = '/MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/30000000/DIGI'
bkPath = '/MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/26104080/XDIGI'
data  = BKQuery(bkPath, dqflag=['OK']).getDataset()
j.inputdata = data[0:50]
#j.inputdata = data[0:1]
#j.inputdata = data
j.backend = Dirac()
j.splitter = SplitByFiles(filesPerJob=3)
#j.outputfiles = [DiracFile('eff_ntuple_HLT1_HLT2_Xic0.root')]
j.outputfiles = [DiracFile('eff_ntuple_HLT1_HLT2_Xic0_NoUT.root')]
j.submit()

