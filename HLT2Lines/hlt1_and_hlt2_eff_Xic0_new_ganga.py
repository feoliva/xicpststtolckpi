###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_moore
from Moore.tcks import dump_hlt2_configuration
from HltEfficiencyChecker.config import run_chained_hlt_with_tuples
from RecoConf.hlt1_allen import sequence as allen_sequence
#from HltEfficiencyChecker.config import run_moore_with_tuples
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.protoparticles import make_charged_protoparticles

from Hlt2Conf.lines.charm.prod_xsec import xic02pkkpi_line

decay = (
    "[${Xic0} Xi_c0 ==> ${p}p+ ${Km}K- ${Km}K- ${pip}pi+]CC"
)


def make_lines():
    return [xic02pkkpi_line()]


options.lines_maker = make_lines

options.input_type = 'ROOT'
options.input_raw_format = 0.3
options.evt_max = -1

from RecoConf.reconstruction_objects import reconstruction


options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'
# options.output_file = 'hlt2_example.dst'
# options.output_type = 'ROOT'

#options.ntuple_file = "eff_ntuple_HLT2.root"
#options.ntuple_file = "eff_ntuple_HLT2_2.root"
#options.ntuple_file = "eff_ntuple_HLT1_HLT2_eos.root"
#options.ntuple_file = "eff_ntuple_HLT1_HLT2_Xic0.root"
options.ntuple_file = "eff_ntuple_HLT1_HLT2_Xic0_NoUT.root"

# needed to run over FTv2 data
from RecoConf.hlt1_tracking import default_ft_decoding_version
#default_ft_decoding_version.global_bind(value=2)
default_ft_decoding_version.global_bind(value=6)

# TODO stateProvider_with_simplified_geom must go away from option files
from RecoConf.global_tools import stateProvider_with_simplified_geom
#with allen_sequence.bind(sequence="hlt1_pp_default"), reconstruction.bind(
#        from_file=False):
# make_charged_protoparticles.bind(            enable_muon_id=True):
from RecoConf.reconstruction_objects import reconstruction as reconstruction
from RecoConf.hlt2_global_reco import reconstruction as reconstruction_from_reco, make_fastest_reconstruction
    
with allen_sequence.bind(sequence="hlt1_pp_default"), reconstruction.bind(from_file=False), reconstruction_from_reco.bind(make_reconstruction=make_fastest_reconstruction),\
     make_fastest_reconstruction.bind(skipUT=True):
    run_chained_hlt_with_tuples(
        options, decay, public_tools=[stateProvider_with_simplified_geom()])
