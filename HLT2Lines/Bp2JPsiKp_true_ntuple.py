"""
Read an HLT2 file and create an ntuple with the new DaVinci runned with gaudirun.py script.
"""
import Functors as F
from FunTuple import FunctorCollection
from FunTuple.functorcollections import Kinematics
from FunTuple import FunTuple_Particles as Funtuple
from PyConf.application import make_data_with_FetchDataFromFile
from DaVinci.Configuration import run_davinci_app
from DaVinci.reco_objects import make_pvs_for
from DaVinci.algorithms import add_filter
from DaVinci import options


def main():
    dp2jpsikp_data = make_data_with_FetchDataFromFile(
        "/Event/HLT2/Hlt2B2CC_BuToJpsiKplus_JpsiToMuMu_TightLine/Particles")
    #"/Event/HLT2/Hlt2B2CC_BuToJpsiKplus_JpsiToMuMu_DetachedLine/Particles")

    fields = {
        "Bp"  : "B+ -> (J/psi(1S) -> mu+ mu-) K+",
        "Jpsi": "B+ -> ^(J/psi(1S) -> mu+ mu-) K+",
        "Mup" : "B+ -> (J/psi(1S) -> ^mu+ mu-) K+",
        "Mum" : "B+ -> (J/psi(1S) -> mu+ ^mu-) K+",
        "Kp"  : "B+ -> (J/psi(1S) -> mu+ mu-) ^K+"
}


# Creating v2 reconstructed vertices to be used in the following functor
#v2_pvs = make_pvs_for(process='Hlt2', data_type="Upgrade")

    v2_pvs = make_pvs_for(process="HLT2")
    parent_variables = FunctorCollection({
       "MAX_PT": F.MAX(F.PT),
       "MIN_PT": F.MIN(F.PT),
       "SUM_PT": F.SUM(F.PT),
       "MAX_P": F.MAX(F.P),
       "MIN_P": F.MIN(F.P),
       "DOCA": F.DOCA(1, 2),
       "DOCACHI2": F.DOCACHI2(1, 2),
       "BPVDIRA": F.BPVDIRA(v2_pvs),
       "CHI2VXNDOF": F.CHI2DOF,
       "BPVFDCHI2": F.BPVFDCHI2(v2_pvs),
       "BPVVDRHO": F.BPVVDRHO(v2_pvs),
       "BPVIPCHI2": F.BPVIPCHI2(v2_pvs),
       "BPVLTIME": F.BPVLTIME(v2_pvs),
       "MAX_BPVIPCHI2": F.MAX(F.BPVIPCHI2(v2_pvs)),
       "MIN_BPVIPCHI2": F.MIN(F.BPVIPCHI2(v2_pvs)),
       "ETA": F.ETA,
       "END_VX": F.END_VX,
       "END_VY": F.END_VY,
       "END_VZ": F.END_VZ

})

    daughter_variables = FunctorCollection({
       "TRCHI2DOF": F.CHI2DOF,
       "TRGHOSTPROB": F.GHOSTPROB,
       "BPVIPCHI2": F.BPVIPCHI2(v2_pvs),
       "PIDK": F.PID_K,
       "PIDe": F.PID_E,
       "PIDmu": F.PID_MU,
       "PIDp": F.PID_P

})

    variables = {
       "Bp": parent_variables + Kinematics(),
       "Jpsi": parent_variables + Kinematics(),
       "Mup": daughter_variables + Kinematics(),
       "Mum": daughter_variables + Kinematics(),
       "Kp": daughter_variables + Kinematics(),
}


#def main():
#    dp2jpsikp_data = make_data_with_FetchDataFromFile(
#        "/Event/HLT2/Hlt2B2CC_BuToJpsiKplus_JpsiToMuMu_TightLine/Particles")
    #"/Event/HLT2/Hlt2B2CC_BuToJpsiKplus_JpsiToMuMu_DetachedLine/Particles")

    my_filter = add_filter("HDRFilter_BptoJpsiKp",
                           "HLT_PASS('Hlt2B2CC_BuToJpsiKplus_JpsiToMuMu_TightLineDecision')")
    #"HLT_PASS('Hlt2B2CC_BuToJpsiKplus_JpsiToMuMu_DetachedLineDecision')")

    my_tuple = Funtuple(
       name="Tuple",
       tuple_name="DecayTree",
       fields=fields,
       variables=variables,
       inputs=dp2jpsikp_data)

    return {"UserAlgs": [my_filter, my_tuple]}, []


options.ntuple_file = "tuple_BptoJpsiKp_10000evts_gaudirun.root"
#options.input_files = ["./hlt2_true_BpToJpsiKp_tight_10000.dst"] #"./hlt2_true_bp_to_jpsikp_detached.dst"
options.input_files = ["/afs/cern.ch/work/f/feoliva/charm-production-run-3_old/options/Moore/b_test.dst"] #"./hlt2_true_bp_to_jpsikp_detached.dst"
options.annsvc_config = "/afs/cern.ch/work/f/feoliva/charm-production-run-3_old/options/Moore/hlt2.tck_blines.json" #"$MYPATH/hlt2_true_bp_to_jpsikp_detached.tck.json"
options.process = 'Hlt2'
options.data_type = "Upgrade"
options.input_type = "ROOT"
options.simulation = True
options.conddb_tag = "sim-20210617-vc-md100"
options.dddb_tag = "dddb-20210617"
#options.input_raw_format = 0.3
options.user_algorithms = "Bp2JPsiKp_true_ntuple:main"
options.write_fsr = False

#fileDB_key = "hlt2_true_bp_to_jpsikp"
#fileDB_path = "./DaVinci_options.yaml"
run_davinci_app() #fileDB_key, fileDB_path)
