###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.hlt1_tracking import require_pvs, require_gec

from Hlt2Conf.standard_particles import (
    make_has_rich_long_protons,
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
)
from Hlt2Conf.algorithms import (
    require_all,
    ParticleCombinerWithPVs,
    ParticleFilterWithPVs,
)
from PyConf import configurable

Lc_mass_range = (2201.0, 2253.0)  # MeV
Lc_mass_range_tight = (2211.0, 2362.0)  # MeV

all_lines = {}


@configurable
def lc2pKpi_combiner_loose(
        proton,
        kaon,
        pion,
        pvs,
        mass_range=Lc_mass_range,
        dau_pT_cut=200,
        dau_mIPchi2DV=6.0,
        comb_cut_one_mIPchi2dv_min=16.0,
        comb_cut_anum2_mIPchi2dv_min=9.0,
        APTsum_cut=3000.0,
        mother_bpvLtime_min=0.00015,
        mother_vfaspf_vchi2pdof_max=10.0,
):
    """
    Port of
    Hlt2CharmHadLcXic2HHHLoose_LcpToKmPpPipCombiner
    """

    descriptor = "[Lambda_c+ -> p+ K- pi+]cc"

    combinecuts = require_all(
        "in_range({mass_range[0]}, AM, {mass_range[1]})",
        "(APT1+APT2+APT3) > {APTsum_cut}",
        "AHASCHILD((MIPCHI2DV(PRIMARY)) > {comb_cut_one_mIPchi2dv_min})",
        "ANUM(MIPCHI2DV(PRIMARY) > {comb_cut_anum2_mIPchi2dv_min}) >= 2",
    ).format(
        mass_range=mass_range,
        comb_cut_one_mIPchi2dv_min=comb_cut_one_mIPchi2dv_min,
        comb_cut_anum2_mIPchi2dv_min=comb_cut_anum2_mIPchi2dv_min,
        APTsum_cut=APTsum_cut,
    )

    mother_cuts = require_all(
        "VFASPF(VCHI2PDOF) < {mother_vfaspf_vchi2pdof_max}",
        "BPVDIRA() > lcldira",
        "BPVLTIME() > {mother_bpvLtime_min}",
    ).format(
        mother_bpvLtime_min=0.00015,
        mother_vfaspf_vchi2pdof_max=10.0,
    )

    daughters_cuts = {}
    for particle in ["p+", "p~-", "K+", "K-", "pi+", "pi-", "mu+"]:
        daughters_cuts[particle] = require_all(
            "PT > {dau_pT_cut}",
            "MIPCHI2DV(PRIMARY) > {dau_mIPchi2DV}").format(
                dau_pT_cut=200,
                dau_mIPchi2DV=6.0,
            )

    return ParticleCombinerWithPVs(
        [proton, kaon, pion],
        pvs,
        DecayDescriptor=descriptor,
        Preambulo=["import math", "lcldira = math.cos( 1.57079632679 )"],
        CombinationCut=combinecuts,
        MotherCut=mother_cuts,
        DaughtersCuts=daughters_cuts,
    )


@configurable
def Lc_filter(
        Lc_loose,
        pvs,
        mass_range=Lc_mass_range,
        sumtree_minPT=3000.0,
        maxtree_PT=1000.0,
        nintree_cut_PT=9.0,
        mintree_cut_PT=200.0,
        mintree_min_mIPchi2dv=6.0,
        maxtree_min_mIPchi2dv=16.0,
        nintree_min_mIPchi2dv=9.0,  # has at least one particle >9.0
        vCHI2pdof_max=10.0,
        bpvLtime_min=0.00015,
):
    filter_cuts = require_all(
        "in_range({mass_range[0]}, M, {mass_range[1]})",
        "(SUMTREE(PT, ISBASIC, 0.0) > {sumtree_minPT})",
        "(MAXTREE(ISBASIC, PT) > {maxtree_PT})",
        "(NINTREE(ISBASIC & (PT > {nintree_cut_PT})) > 1)",
        "(MINTREE(ISBASIC, PT) > {mintree_cut_PT})",
        "(MINTREE(ISBASIC, MIPCHI2DV(PRIMARY)) > {mintree_min_mIPchi2dv})",
        "(MAXTREE(ISBASIC, MIPCHI2DV(PRIMARY)) > {maxtree_min_mIPchi2dv})",
        "(NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > {nintree_min_mIPchi2dv})) > 1)",
        "(VFASPF(VCHI2PDOF) < {vCHI2pdof_max})",
        "(BPVDIRA() > lcldira)",
        "(BPVLTIME() > {bpvLtime_min})",
    ).format(
        mass_range=mass_range,
        sumtree_minPT=sumtree_minPT,
        maxtree_PT=maxtree_PT,
        nintree_cut_PT=nintree_cut_PT,
        mintree_cut_PT=mintree_cut_PT,
        mintree_min_mIPchi2dv=mintree_min_mIPchi2dv,
        maxtree_min_mIPchi2dv=maxtree_min_mIPchi2dv,
        nintree_min_mIPchi2dv=nintree_min_mIPchi2dv,
        vCHI2pdof_max=vCHI2pdof_max,
        bpvLtime_min=bpvLtime_min,
    )

    return ParticleFilterWithPVs(
        Lc_loose,
        pvs,
        Preambulo=["import math", "lcldira = math.cos( 1.57079632679 )"],
        Code=filter_cuts,
    )


@configurable
def Lc_mass_filter(
        Lc_tight,
        pvs,
        mass_range_tight=Lc_mass_range_tight,
):
    mass_filter = require_all(
        "in_range({mass_range_tight[0]}, M, {mass_range_tight[1]})", ).format(
            mass_range_tight=mass_range_tight)
    return ParticleFilterWithPVs(Lc_tight, pvs, Code=mass_filter)


@register_line_builder(all_lines)
@configurable
def lc2pKpi_line(name="Hlt2CharmHadLcpToPpKmPipLine", prescale=1):
    pvs = make_pvs()
    protons = make_has_rich_long_protons()
    kaons = make_has_rich_long_kaons()
    pions = make_has_rich_long_pions()

    Lc = Lc_mass_filter(
        Lc_filter(lc2pKpi_combiner_loose(protons, kaons, pions, pvs), pvs),
        pvs)

    # TODO: should the prefilter have its own make_pvs() returned object?
    #       or should I reuse pvs?
    prefilters = [require_gec(), require_pvs(make_pvs())]

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + prefilters + [Lc],
        prescale=prescale,
    )

