
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from grpc import channel_ready_future
from Moore import options, run_moore
from Moore.tcks import dump_hlt2_configuration
from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import (
    reconstruction as hlt2_reconstruction,
    make_fastest_reconstruction,
)

#from Hlt2Conf.lines.charm.prod_xsec import (lcp2pkpi_line, xic02pkkpi_line, xicp2pkpi_line, xsec_make_lc_or_xicp_pKpi, xsec_make_xic0_pkkpi)
from Hlt2Conf.lines.b_to_charmonia.hlt2_b2cc import BuToJpsimumuKplus_tight_line, BuToJpsimumuKplus_detached_line
from GaudiKernel.SystemOfUnits import MeV, GeV, picosecond, mm, mrad
import math



b_lines = (BuToJpsimumuKplus_tight_line, BuToJpsimumuKplus_detached_line)

def make_lines():
    return [line() for line in b_lines]

public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]


from RecoConf.hlt1_tracking import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)

options.input_files = [
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00143567/0000/00143567_00000021_1.xdigi",
]
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'

options.input_type = 'ROOT'
options.input_raw_format = 0.3
options.output_file = 'b_test.dst'
options.output_type = 'ROOT'

options.evt_max = 1000



with reconstruction.bind(from_file=False), hlt2_reconstruction.bind(
    make_reconstruction=make_fastest_reconstruction
), make_fastest_reconstruction.bind(skipUT=True):
    config = run_moore(options, make_lines, public_tools)

dump_hlt2_configuration(config, "hlt2.tck_blines.json")

