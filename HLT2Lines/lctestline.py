from Moore import options, run_moore
from Hlt2Conf.lines.charm.lc_to_pkpi import lc2pKpi_line
from RecoConf.global_tools import stateProvider_with_simplified_geom
from Moore.tcks import dump_hlt2_configuration


def all_lines():
    return [lc2pKpi_line()]

public_tools = [stateProvider_with_simplified_geom()]

input_files = [

   "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145799/0000/00145799_00000077_1.xdigi"
]

options.input_files = input_files
options.input_type = 'ROOT'
options.input_raw_format = 0.3

options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'
options.evt_max = -1


from RecoConf.reconstruction_objects import reconstruction

with reconstruction.bind(from_file=False): 
    config=run_moore(options, all_lines, public_tools)

dump_hlt2_configuration(config, "hlt2.tck.json")

