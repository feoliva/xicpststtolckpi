###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from DecayTreeTuple import DecayTreeTupleTruthUtils, Configuration
from PhysSelPython.Selections import AutomaticData
import json
from Configurables import (
    DecayTreeTuple,
    ApplicationMgr,
    createODIN,
    HltANNSvc,
    HltDecReportsDecoder,
    LHCb__UnpackRawEvent,
)
from GaudiConf import reading


# Helper functions to be able to read the Hlt2 data
def get_hlt2_unpackers(is_simulation):
    """Configures algorithms for reading HLT2 output.

    This is a temporary measure until support for Run 3 HLT2 output is added to
    an LHCb application.
    """
    unpacker = LHCb__UnpackRawEvent(
        "UnpackRawEvent",
        BankTypes=["ODIN"],
        RawBankLocations=["/Event/DAQ/RawBanks/ODIN"],
    )

    dec_reports = HltDecReportsDecoder(
        SourceID="Hlt2", OutputHltDecReportsLocation="/Event/Hlt2/DecReports"
    )
    reading_algs = (
        [reading.decoder()]
        + reading.unpackers()
        + [dec_reports]
        + [unpacker, createODIN()]
    )
    if is_simulation:
        reading_algs = reading.mc_unpackers() + reading_algs
    return reading_algs


def configure_packed_locations(tck_location):
    """Configures HltANNSvc to know about packed locations and hlt2 decision names used in Moore.

    tck_location (string): Location of json file containing trigger configuration.

    """

    with open(tck_location) as f:
        tck = json.load(f)
    ann_config = tck["HltANNSvc/HltANNSvc"]
    HltANNSvc(
        PackedObjectLocations={
            str(k): v for k, v in ann_config["PackedObjectLocations"].items()
        }
    )
    HltANNSvc(
        Hlt2SelectionID={str(k): v for k, v in ann_config["Hlt2SelectionID"].items()}
    )


# TODO: could get most of the following from yaml
# The output of the hlt2 line - full path
#line = AutomaticData("/Event/HLT2/Hlt2CharmD0ToKmPipLine/Particles")
line = AutomaticData("/Event/HLT2/Hlt2CharmHadLcpToPpKmPipLine/Particles")

branches = {
    "Lc": "^[Lambda_c+ -> p+  K- pi+]CC",
    "Lc_p": "[Lambda_c+ -> ^p+  K- pi+]CC",
    "Lc_Km": "[Lambda_c+ -> p+  ^K- pi+]CC",
    "Lc_pip": "[Lambda_c+ -> p+  K- ^pi+]CC",
}

dtt = DecayTreeTuple("Tuple")
dtt.Inputs = [line.outputLocation()]
dtt.Decay = "[Lambda_c+ -> ^p+  ^K- ^pi+]CC"
dtt.addBranches(branches)

relations = [
    "Relations/ChargedPP2MCP",
    "Relations/NeutralPP2MCP",
]
mc_tools = ["MCTupleToolHierarchy"]

dtt.ToolList = ["TupleToolMCBackgroundInfo", "TupleToolMCTruth"]

stream = "/Event/HLT2"
DecayTreeTupleTruthUtils.makeTruth(dtt, relations, mc_tools)
dtt.TupleToolMCTruth.DaVinciSmartAssociator.RootInTES = stream
dtt.TupleToolMCBackgroundInfo.BackgroundCategory.RootInTES = stream

lc_hybrid = dtt.Lc.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Lc")
pplus_hybrid = dtt.pplus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_pplus")
piplus_hybrid = dtt.piplus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_piplus")
Kminus_hybrid = dtt.Kminus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Kminus")

lc_hybrid.Variables = {
    "MAX_PT_CHILD": "MAXTREE(ISBASIC, PT, -1)",
    "DOCA12": "DOCA(1,2)",
    "BPVDIRA": "BPVDIRA()",
    "CHI2VXNDOF": "CHI2VXNDOF",
    "BPVVDCHI2": "BPVVDCHI2()",
    "M": "M",
    "PT": "PT",
    "P": "P",
    "BPVIPCHI2": "BPVIPCHI2()",
}
daughter_vars = {
    "PT": "PT",
    "P": "P",
    "TRCHI2DOF": "TRCHI2DOF",
    "TRGHOSTPROB": "TRGHOSTPROB",
    # "MIPCHI2DV": "MIPCHI2DV(PRIMARY)",  used in TriggerLine but effectively same as BPVIPCHI2
    "BPVIPCHI2": "BPVIPCHI2()",
    "PIDK": "PIDK",
    "PIDe": "PIDe",
    "PIDmu": "PIDmu",
}
Kminus_hybird.Variables = daughter_vars
piplus_hybrid.Variables = daughter_vars
pplus_hybrid.Variables = daughter_vars

# Load the 'TCK's dumped from the HLT2 Moore job
configure_packed_locations("hlt2.tck.json")

# Configure the unpacking of data (we assume we want MC information)
# and the running of the user algorithms. The order is important.
user_algs = [dtt]
ApplicationMgr().TopAlg = get_hlt2_unpackers(is_simulation=True) + user_algs
