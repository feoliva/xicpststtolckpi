###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_moore
from HltEfficiencyChecker.config import run_moore_with_tuples
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.protoparticles import make_charged_protoparticles

decay = (
    "[${Lc} Lambda_c+ => ${p}p+ ${Km}K- ${pip}pi+]CC"
)
options.input_files = [
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145799/0000/00145799_00000077_1.xdigi"
]
options.input_type = 'ROOT'
options.input_raw_format = 0.3
options.evt_max = -1

from RecoConf.reconstruction_objects import reconstruction


options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'
# options.output_file = 'hlt2_example.dst'
# options.output_type = 'ROOT'

#options.ntuple_file = "eff_ntuple_HLT2.root"
#options.ntuple_file = "eff_ntuple_HLT2_2.root"
options.ntuple_file = "eff_ntuple_HLT2_3.root"

# needed to run over FTv2 data
from RecoConf.hlt1_tracking import default_ft_decoding_version
#default_ft_decoding_version.global_bind(value=2)
default_ft_decoding_version.global_bind(value=6)

# TODO stateProvider_with_simplified_geom must go away from option files
from RecoConf.global_tools import stateProvider_with_simplified_geom
with reconstruction.bind(from_file=False), make_charged_protoparticles.bind(
        enable_muon_id=True):
    run_moore_with_tuples(
        options,
        False,
        decay,
        public_tools=[stateProvider_with_simplified_geom()])
