###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#from Moore import options, run_moore
from Moore import options
#from Moore.tcks import dump_hlt2_configuration
from HltEfficiencyChecker.config import run_chained_hlt_with_tuples
from RecoConf.hlt1_allen import sequence as allen_sequence
#from HltEfficiencyChecker.config import run_moore_with_tuples
from RecoConf.reconstruction_objects import reconstruction
#from RecoConf.protoparticles import make_charged_protoparticles
#from Hlt2Conf.lines.charm.lc_to_pkpi import all_lines
from Hlt2Conf.lines.charm.prod_xsec import all_lines
from RecoConf.hlt2_global_reco import (
    reconstruction as hlt2_reconstruction,
    make_fastest_reconstruction,
)


decay = (
    "[${Xicp} Xi_c+ ==> ${p}p+ ${Km}K- ${pip}pi+]CC"
)


def make_lines():
    return [builder() for builder in all_lines.values()]


options.lines_maker = make_lines



#options.input_files = glob.glob(
options.input_files = [ 
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000095_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000124_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000080_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000041_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000125_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000126_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000122_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000100_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000112_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000130_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000109_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000030_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000068_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000033_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000118_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000010_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000082_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000098_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000032_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000104_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000164_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000161_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000142_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000150_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000143_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000195_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000196_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000190_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000193_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000183_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000204_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000001_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000006_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000009_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000070_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000029_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000078_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000137_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000139_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000022_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000013_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000077_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000075_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000136_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000105_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000018_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000129_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000069_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000131_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000119_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000106_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000020_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000179_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000174_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000168_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000157_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000180_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000144_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000151_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000197_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000186_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000184_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000192_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000209_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000008_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000056_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000035_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000097_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000099_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000014_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000079_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000023_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000093_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000026_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000113_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000117_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000121_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000012_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000067_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000031_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000025_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000071_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000178_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000149_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000004_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000089_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000037_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000040_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000116_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000076_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000066_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000103_1.xdigi",
      "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00145795/0000/00145795_00000128_1.xdigi",

]
options.input_type = 'ROOT'
options.input_raw_format = 0.3
#options.evt_max = -1
options.evt_max = 30000
#options.evt_max = 10000

from RecoConf.reconstruction_objects import reconstruction


options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'
# options.output_file = 'hlt2_example.dst'
# options.output_type = 'ROOT'

#options.ntuple_file = "/eos/lhcb/user/f/feoliva/HLT2lines/Efficiency/Efficiency_HLT1Charm/eff_ntuple_HLT2_noTrk_newCharmLine.root"
#options.ntuple_file = "/eos/lhcb/user/f/feoliva/HLT2lines/Efficiency/Efficiency_HLT1Charm/eff_ntuple_HLT2_noTrk_newCharmLine_Xic+.root"
#options.ntuple_file = "/eos/lhcb/user/f/feoliva/HLT2lines/Efficiency/Efficiency_HLT1Charm/eff_ntuple_HLT2_noTrk_newCharmLine_Xic+_test.root"
#options.ntuple_file = "/eos/lhcb/user/f/feoliva/HLT2lines/Efficiency/Efficiency_HLT1Charm/eff_ntuple_HLT2_newCharmLine_Xic+.root"
#options.ntuple_file = "/eos/lhcb/user/f/feoliva/HLT2lines/Efficiency/Efficiency_HLT1Charm/eff_ntuple_HLT2_HLT1CharmLine_Xic+.root"
#options.ntuple_file = "/eos/lhcb/user/f/feoliva/HLT2lines/Efficiency/Efficiency_HLT1Charm/eff_ntuple_HLT2_HLT1CharmLine_Xic+_30kev.root"
options.ntuple_file = "/eos/lhcb/user/f/feoliva/HLT2lines/Efficiency/Efficiency_HLT1Charm/eff_ntuple_HLT2_HLT1CharmLine_Xic+_30kevents.root"

#options.ntuple_file = "eff_ntuple_HLT2_2.root"
#options.ntuple_file = "eff_ntuple_HLT1_HLT2_eos.root"
#options.ntuple_file = "eff_ntuple_HLT1_HLT2_LowPTline.root"

# needed to run over FTv2 data
from RecoConf.hlt1_tracking import default_ft_decoding_version
#from RecoConf.hlt1_decoders import default_ft_decoding_version
#default_ft_decoding_version.global_bind(value=2)
default_ft_decoding_version.global_bind(value=6)

# TODO stateProvider_with_simplified_geom must go away from option files
from RecoConf.global_tools import stateProvider_with_simplified_geom

#In case of fast Reco, skipping UT, please use the first lines below
with allen_sequence.bind(sequence="hlt1_pp_veloSP"), reconstruction.bind(
        from_file=False), hlt2_reconstruction.bind(
    make_reconstruction=make_fastest_reconstruction
), make_fastest_reconstruction.bind(skipUT=True):


#with allen_sequence.bind(sequence="hlt1_pp_veloSP"), reconstruction.bind(
#        from_file=False):
    run_chained_hlt_with_tuples(
        options, decay, public_tools=[stateProvider_with_simplified_geom()])



