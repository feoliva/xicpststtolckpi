from Configurables import (
    ApplicationMgr,
    DecayTreeTuple,
    LHCbApp,
    LHCb__UnpackRawEvent,
    createODIN,
    HltANNSvc,
    HltDecReportsDecoder,
    TupleToolRecoStats,
    TupleToolDecay,
)
from Gaudi.Configuration import appendPostConfigAction
from DecayTreeTuple import Configuration
from PhysSelPython.Selections import (
    AutomaticData,
    CombineSelection,
    SelectionSequence,
)
from PhysConf.Selections import TupleSelection

from GaudiConf import reading
import json

# Helper functions to be able to read the Hlt2 data
def get_hlt2_unpackers(is_simulation):
    """Configures algorithms for reading HLT2 output.

    This is a temporary measure until support for Run 3 HLT2 output is added to
    an LHCb application.
    """
    unpacker = LHCb__UnpackRawEvent(
        'UnpackRawEvent',
        BankTypes=['ODIN'],
        RawBankLocations=['/Event/DAQ/RawBanks/ODIN'])

    dec_reports = HltDecReportsDecoder(
        SourceID='Hlt2', OutputHltDecReportsLocation="/Event/Hlt2/DecReports")
    reading_algs = ([reading.decoder()] + reading.unpackers() + [dec_reports] +
                    [unpacker, createODIN()])
    if is_simulation:
        reading_algs = reading.mc_unpackers() + reading_algs
    return reading_algs


def configure_packed_locations(tck_location):
    """Configures HltANNSvc to know about packed locations and hlt2 decision names used in Moore.

    tck_location (string): Location of json file containing trigger configuration.

    """

    with open(tck_location) as f:
        tck = json.load(f)
    ann_config = tck["HltANNSvc/HltANNSvc"]
    HltANNSvc(PackedObjectLocations={
        str(k): v
        for k, v in ann_config["PackedObjectLocations"].items()
    })
    HltANNSvc(Hlt2SelectionID={
        str(k): v
        for k, v in ann_config["Hlt2SelectionID"].items()
    })




all_sequences = []
toolList = [
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolANNPID",
    "TupleToolGeometry",
    "TupleToolPrimaries",
    "TupleToolTrackInfo",
    "TupleToolEventInfo",
]


Lc_LoKiVariables = {
    "BPVVDCHI2": "BPVVDCHI2",
    "BPVIPCHI2": "BPVIPCHI2()",
    # DOCA information
    "DOCAMAX": "DOCAMAX",
    "DOCAMIN": "LoKi.Particles.PFunA(AMINDOCA('LoKi::TrgDistanceCalculator'))",
    "DOCACHI2MAX": "DOCACHI2MAX",
    "DOCA12": "DOCA(1,2)",
    "DOCA13": "DOCA(1,3)",
    "DOCA23": "DOCA(2,3)",
    "Y": "Y",
    "ETA": "ETA",
    "SUM_PT": "(CHILD(PT, 1) + CHILD(PT, 2) + CHILD(PT, 3))",
    "M12": "M12",
    "M23": "M23",
    "M13": "M13",
}


# The output of the HLT2 lineLcp = AutomaticData("/Event/HLT2/Hlt2CharmHadLcpToPpKmPipLine/Particles")
Lcp = AutomaticData("/Event/HLT2/Hlt2CharmHadLcpToPpKmPipLine/Particles")

def LcaddLoki(dtt, branch_name):
    branch = getattr(dtt, branch_name)
    branch.addTupleTool(
        "LoKi::Hybrid::TupleTool/LoKi/%s" % branch_name
    ).Variables = dict(**Lc_LoKiVariables)


## Lambda_c tree

tupSel = TupleSelection(
    "Lc2PKPi",
    [Lcp],
    Decay="[Lambda_c+ -> ^p+  ^K- ^pi+]CC",
    Branches={
        "Lc": "^[Lambda_c+ -> p+  K- pi+]CC",
        "Lc_p": "[Lambda_c+ -> ^p+  K- pi+]CC",
        "Lc_Km": "[Lambda_c+ -> p+  ^K- pi+]CC",
        "Lc_pip": "[Lambda_c+ -> p+  K- ^pi+]CC",
    },
)

Lc_dtt = tupSel.algorithm()
Lc_dtt.ToolList = toolList[:]
Lc_dtt.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
Lc_dtt.TupleToolRecoStats.Verbose = True
Lc_dtt.addTool(TupleToolDecay)
LcaddLoki(Lc_dtt, "Lc")
Lc_dtt.ToolList += ["TupleToolMCBackgroundInfo", "TupleToolMCTruth"]


all_sequences += [SelectionSequence("Lc2PKPi_seq", tupSel).sequence()]


LHCbApp().DataType = "Upgrade"
user_algs = all_sequences
LHCbApp().TupleFile = "hlt2_example.root"

# Load the 'TCK' dumped from the Moore job, assuming the TCK file was named
# like the Moore output file
configure_packed_locations("hlt2.tck.json")

# Configure the unpacking of data (we assume we want MC information)
# and the running of the user algorithms. The order is important.
ApplicationMgr().TopAlg = get_hlt2_unpackers(is_simulation=True) + user_algs

